package client

var getProfile = `
	query {
		result: getProfile {
			name {
				firstName
				lastName
			}
			contact {
				phone
				email
			}
			isPhoneVerified
			publicProfileId
			userType
			isActive
			gender
			images {
				profileImage
				coverImage
			}
			uidx
			bio
			dob
			location
			isNewUser
			myntraInsider {
				status
				loyaltyPoints
				tierName
			}
		}
	}
`
