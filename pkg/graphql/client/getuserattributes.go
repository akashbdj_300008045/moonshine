package client

var getUserAttributes = `
	query {
		result: getUserAttributes { 
			hasPurchased
			defaultPincode
			shippedMobileNumbers
			myntraInsider
		}
	}
`
