package client

var search = `
	query(
		$req_url_q: String!
		$req_query_o: Int
		$req_query_rows: Int
		$req_query_f: String
		$req_query_rf: String
		$req_query_sort: String
		$req_query_storefrontId: String
		$req_query_pincode: String
		$req_query_geoF: String
		$req_query_extra_search_param: String
		$req_query_pinProductsCsv: String
		$req_query_returnDocs: String
		$req_query_searchRequestId: String
		$req_query_isFacet: String
		$req_query_userQuery: String
		$req_query_isLuxury: String
		$req_query_isInlineFilterAvailable: String
		$req_query_ifo: Int
		$req_query_ifc: Int
		$req_query_totalPLAShown: Int
		$req_query_totalPLACount: Int
		$req_query_id: Int
		$req_query_type: String
		$req_query_priceBuckets: Int
		$req_query_requestType: String
		$req_query_selectedCluster: String
		$req_query_productsPerCluster: Int
		$req_query_clusterRows: Int
	) {
  		result: search(
			  q: $req_url_q
			  offset: $req_query_o
			  rows: $req_query_rows
			  filters: $req_query_f
			  rangeFilters: $req_query_rf
			  sort: $req_query_sort
			  storefrontId: $req_query_storefrontId
			  pincode: $req_query_pincode
			  geoF: $req_query_geoF
			  extraSearchParam: $req_query_extra_search_param
			  pinProductsCsv: $req_query_pinProductsCsv
			  returnDocs: $req_query_returnDocs
			  searchRequestId: $req_query_searchRequestId
			  isFacet: $req_query_isFacet
			  userQuery: $req_query_userQuery
			  isLuxury: $req_query_isLuxury
			  isInlineFilterAvailable: $req_query_isInlineFilterAvailable
			  ifo: $req_query_ifo
			  ifc: $req_query_ifc
			  totalPLAShown: $req_query_totalPLAShown
			  totalPLACount: $req_query_totalPLACount
			  id: $req_query_id
			  type: $req_query_type
			  priceBuckets: $req_query_priceBuckets
			  requestType: $req_query_requestType
			  selectedCluster: $req_query_selectedCluster
			  productsPerCluster: $req_query_productsPerCluster
			  clusterRows: $req_query_clusterRows
		) {
			responseType
			totalCount
			totalPLAShown
			totalPLACount
			filters {
				primaryFilters {
					id
					filterValues {
						id
						count
						meta
						pLevel
					}
				}
				secondaryFilters {
					id
					filterValues {
						id
						count
						pLevel
						src
					}
					isVisual
				}
				rangeFilters {
					id
					gap
					start
					end
					filterValues {
						id
						count
						start
						end
						pLevel
					}
				}
				geoFilters {
					id
					filterValues {
						minDays
						maxDays
						num
						pLevel
					}
				}
				inlineFilters {
					position {
						afterProducts
					}
					id
					filterValues {
						id
						count
						meta
						pLevel
						src
					}
					isVisual
				}
			}
			products {
				landingPageUrl
				loyaltyPointsEnabled
				adId
				isPLA
				productId
				product
				productName
				rating
				ratingCount
				isFastFashion
				futureDiscountedPrice
				futureDiscountStartDate
				discount
				brand
				searchImage
				effectiveDiscountPercentageAfterTax
				effectiveDiscountAmountAfterTax
				productVideos {
					videoUrl
					videoHost
					videoId
				}
				inventoryInfo {
					skuId
					label
					inventory
					available
				}
				sizes
				images {
					view
					src
				}
				gender
				primaryColour
				discountLabel
				discountDisplayLabel
				additionalInfo
				category
				mrp
				price
				advanceOrderTag
				colorVariantAvailable
				productimagetag
				listViews
				discountType
				tdBxGyText
				catalogDate
				season
				year
				isPersonalised
				eorsPicksTag
				personalizedCoupon
				personalizedCouponValue
				productMeta
				systemAttributes {
					attribute
					value
				}
			}
			sortOptions
			nextBestResults {
				listURL
				displayName
				totalProductsCount
				products {
					discount
					brand
					searchImage
					price
					mrp
					gender
					sizes
					additionalInfo
					productName
					product
					category
					primaryColour
					discountLabel
					productId
					discountDisplayLabel
					allSkuForSizes
					imageEntires
					inventoryInfo {
						skuid
						size
						inventory
						available
					}
					images {
						view
						src
						domain
						securedDomain
						relativePath
						serverUploaderType
					}
					landingPageUrl
					futureDiscountedPrice
					colorVariantAvailable
					listViews
					discountType
					systemAttributes {
						attribute
						value
					}
				}
			}
			pageTitle
			storefrontId
			guidedNavigation {
				name
				guidedNavEntries {
					key
					value
					img
					numProducts
					isAutocreated
					rank
				}
			}
			upsInfo {
				personalizationSortAlgoUsed
				numPersonalizedProductShown
				isPersonalized
			}
			changeLog {
				originalQuery
				changedQuery
				searchComponent
			}
			appliedParams {
				filters {
					id
					values
				}
				geoFilters {
					id
					values
				}
				rangeFilters {
					id
					values {
						id
						start
						end
					}
				}
				sort
			}
			templateMessage
			querySubstitution {
				templateId
				inputQuery
				suggestedQuery
				displayText
				resolvedQuery {
					id
					type
				}
			}
			totalProductCount
			clusters {
				name
				id
				selected
			}
			selectedClusterValues {
				name
				totalCount
				filterParams
				products {
					landingPageUrl
					loyaltyPointsEnabled
					productId
					product
					productName
					futureDiscountedPrice
					futureDiscountStartDate
					discount
					brand
					searchImage
					inventoryInfo {
						skuId
						label
						inventory
						available
					}
					sizes
					images {
						view
						src
					}
					gender
					primaryColour
					discountLabel
					discountDisplayLabel
					additionalInfo
					category
					mrp
					price
					advanceOrderTag
					colorVariantAvailable
					productimagetag
					listViews
					discountType
					tdBxGyText
					catalogDate
					season
					year
					eorsPicksTag
					isPersonalised
				}
			}
		}
	}
`
