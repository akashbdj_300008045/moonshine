package client

var Queries = map[string]string{
	"search":            search,
	"getUserAttributes": getUserAttributes,
	"getProfile":        getProfile,
	"addAddress":        addAddress,
}
