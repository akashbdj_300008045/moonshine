package client

var addAddress = `
	mutation ($req_body: AddressInput!) { 
		result: addAddress(adr: $req_body) { 
			id
			isDefault
			checkoutAllowed
			addressType
			notAvailableDays
			streetAddress
			locality
			city
			pincode
			state { 
				code
				name
			}
			country {
				code
				name
			}
			user {
				uidx
				name
				email
				mobile
			}
		}
	}
`
