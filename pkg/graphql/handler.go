package graphql

import (
	"context"
	"fmt"
	"net/http"

	"bitbucket.org/myntra/moonshine/pkg/graphql/client"
	"bitbucket.org/myntra/moonshine/pkg/util/errors"

	"github.com/99designs/gqlgen/graphql"
	"github.com/vektah/gqlparser/ast"
	"github.com/vektah/gqlparser/gqlerror"
	"github.com/vektah/gqlparser/parser"
	"github.com/vektah/gqlparser/validator"
)

type Handler struct {
	Exec graphql.ExecutableSchema
}

func LoadSchema(conf Config) *Handler {
	return &Handler{
		Exec: NewExecutableSchema(conf),
	}
}

func (h *Handler) Execute(ctx context.Context, queryIdent string, operationName string, variables map[string]interface{}) ([]byte, *errors.CustomError) {
	var doc *ast.QueryDocument
	query := client.Queries[queryIdent]

	// tracing operation missing
	doc, gqlErr := parser.ParseQuery(&ast.Source{Input: query})
	if gqlErr != nil {
		// handle parsing error: http.StatusUnprocessableEntity
		//fmt.Println("[GraphQL Handler ERROR] Query Parsing  ", gqlErr)
		return nil, errors.CreateError(1001, "error in parsing query", http.StatusInternalServerError)
	}

	// cache it like in gqlgen
	listErr := validator.Validate(h.Exec.Schema(), doc)
	if len(listErr) != 0 {
		// handle errors
		//fmt.Println("[GraphQL Handler ERROR] Validating Doc with Schema  ", listErr)
		return nil, errors.CreateError(1001, "error in validating schema", http.StatusInternalServerError)
	}

	// tracing missing here also
	op := doc.Operations.ForName(operationName)
	if op == nil {
		// do we need this check?
		//fmt.Println("[GraphQL Handler] Operation Definition is nil  ")
	}

	vars, err := validator.VariableValues(h.Exec.Schema(), op, variables)
	if err != nil {
		// return errors with proper messages!
		//fmt.Println("[GraphQL Handler ERROR] Validator Variable Parsing ", err)
		errors.CreateError(1001, "error in validating variables values", http.StatusBadRequest)
	}

	reqCtx := graphql.NewRequestContext(doc, query, vars)
	reqCtx.ErrorPresenter = errorHook
	reqCtx.ResolverMiddleware = resolverHook
	ctx = graphql.WithRequestContext(ctx, reqCtx)

	var res *graphql.Response
	switch op.Operation {
	case ast.Query:
		res = h.Exec.Query(ctx, op)
	case ast.Mutation:
		res = h.Exec.Mutation(ctx, op)
	default:
		//sendErrorf(w, http.StatusBadRequest, "unsupported operation type")
		//fmt.Println("[GraphQL Handler BAD REQUEST] unsupported operation", err)
		return nil, errors.CreateError(1001, "unsupported operation", http.StatusBadRequest)
	}

	if res == nil {
		// TODO: check how it should be handled
		return nil, nil
	}

	// routePath := chi.RouteContext(ctx).RoutePattern()
	for _, err := range res.Errors {
		fmt.Println("message ", err.Message)
		// fmt.Println("loc ", err.Locations)
		// fmt.Println("path ", err.Path)
		// fmt.Println("rule ", err.Rule)
		// TODO: how we can handle custom errors here?
		return nil, errors.CreateError(1001, "errors inside graphql response", http.StatusInternalServerError)
	}

	b, e := res.Data.MarshalJSON()
	if e != nil {
		// TODO: Panic?
		//panic(e)
		return nil, errors.CreateError(1001, "error in marshalling graphql response", http.StatusInternalServerError)
	}

	return b[10 : len(b)-1], nil
}

func errorHook(ctx context.Context, e error) *gqlerror.Error {
	// any special logic you want to do here. Must specify path for correct null bubbling behaviour.
	if myError, ok := e.(*errors.CustomError); ok {
		return gqlerror.ErrorPathf(graphql.GetResolverContext(ctx).Path(), myError.Message)
	}

	return graphql.DefaultErrorPresenter(ctx, e)
}

func resolverHook(ctx context.Context, next graphql.Resolver) (interface{}, error) {
	// rc := graphql.GetResolverContext(ctx)
	res, err := next(ctx)
	return res, err
}
