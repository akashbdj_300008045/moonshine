package resolvers

import (
	"bitbucket.org/myntra/moonshine/pkg/graphql"
)

var _ graphql.QueryResolver = &queryResolver{}

type queryResolver struct{ *Resolver }
