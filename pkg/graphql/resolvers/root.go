package resolvers

import (
	"bitbucket.org/myntra/moonshine/pkg/graphql"
)

// Root Resolver
type Resolver struct{}

func New() *Resolver {
	return &Resolver{}
}

func (r *Resolver) Query() graphql.QueryResolver {
	return &queryResolver{r}
}

func (r *Resolver) Mutation() graphql.MutationResolver {
	return &mutationResolver{r}
}

func (r *Resolver) Address() graphql.AddressResolver {
	return &addressResolver{r}
}

func (r *Resolver) Search() graphql.SearchResolver {
	return &searchResolver{r}
}

func (r *Resolver) SearchProduct() graphql.SearchProductResolver {
	return &searchProductResolver{r}
}
