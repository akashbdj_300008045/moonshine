package resolvers

import (
	"context"

	"bitbucket.org/myntra/moonshine/pkg/config"
	"bitbucket.org/myntra/moonshine/pkg/graphql"
	"bitbucket.org/myntra/moonshine/pkg/models"
	"bitbucket.org/myntra/moonshine/pkg/service/address"
)

var _ graphql.AddressResolver = &addressResolver{}

type addressResolver struct{ *Resolver }

func (*mutationResolver) AddAddress(ctx context.Context, adr models.AddressInput) (*address.Address, error) {
	p := address.Payload{
		IsDefault:        adr.IsDefault,
		AddressType:      adr.AddressType,
		StreetAddress:    adr.StreetAddress,
		Locality:         adr.Locality,
		City:             adr.City,
		Pincode:          adr.Pincode,
		StateCode:        adr.State.Code,
		StateName:        adr.State.Name,
		CountryCode:      adr.Country.Code,
		Name:             adr.Name,
		Email:            adr.Email,
		Mobile:           adr.Mobile,
		NotAvailableDays: adr.NotAvailableDays,
	}

	r, err := address.Post(ctx, p, getUrl("v3"))
	if err != nil {
		return nil, err
	}

	return &r.Address[0], nil
}

func (r *addressResolver) Country(ctx context.Context, obj *address.Address) (*models.Country, error) {
	c := &models.Country{Code: obj.CountryCode, Name: obj.CountryName}
	return c, nil
}

func (r *addressResolver) User(ctx context.Context, obj *address.Address) (*models.User, error) {
	u := &models.User{
		Uidx:   obj.Uidx,
		Name:   obj.Name,
		Email:  obj.Email,
		Mobile: obj.Mobile,
	}
	return u, nil
}

func (r *addressResolver) State(ctx context.Context, obj *address.Address) (*models.State, error) {
	s := &models.State{Code: obj.StateCode, Name: obj.StateName}
	return s, nil
}

func getUrl(version string) string {
	return config.GetUrl("address") + version + "/address/"
}
