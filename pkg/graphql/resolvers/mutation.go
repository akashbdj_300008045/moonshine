package resolvers

import "bitbucket.org/myntra/moonshine/pkg/graphql"

var _ graphql.MutationResolver = &mutationResolver{}

type mutationResolver struct{ *Resolver }
