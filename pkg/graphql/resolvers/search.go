package resolvers

import (
	"context"
	"net/http"
	"strconv"

	"bitbucket.org/myntra/moonshine/pkg/graphql"
	"bitbucket.org/myntra/moonshine/pkg/service/search"
	"bitbucket.org/myntra/moonshine/pkg/util/helpers"
)

var _ graphql.SearchResolver = &searchResolver{}
var _ graphql.SearchProductResolver = &searchProductResolver{}

type searchResolver struct{ *Resolver }
type searchProductResolver struct{ *Resolver }

func (*queryResolver) Search(ctx context.Context, q string, offset *int, rows *int, filters *string, rangeFilters *string, sort *string, storefrontID *string, pincode *string, geoF *string, extraSearchParam *string, pinProductsCsv *string, returnDocs *string, searchRequestID *string, isFacet *string, userQuery *string, isLuxury *string, isInlineFilterAvailable *string, ifo *int, ifc *int, totalPlashown *int, totalPlacount *int, id *int, typeArg *string, priceBuckets *int, requestType *string, selectedCluster *string, productsPerCluster *int, clusterRows *int) (*search.Search, error) {

	_isLuxry := false
	if *isLuxury == "true" {
		_isLuxry = true
	}

	_rDocs := true
	if *returnDocs == "false" {
		_rDocs = false
	}

	_isIFA := false
	if *isInlineFilterAvailable == "true" {
		_isIFA = true
	}

	_userQuery := false
	if *userQuery == "true" {
		_userQuery = true
	}

	_isFacet := true
	if *isFacet == "false" {
		_isFacet = false
	}

	srp := &search.SearchRequestParams{
		Query:                   q,
		Offset:                  offset,
		Rows:                    rows,
		Filters:                 filters,
		RangeFilters:            rangeFilters,
		Sort:                    sort,
		StorefrontId:            storefrontID,
		Pincode:                 pincode,
		GeoF:                    geoF,
		ExtraSearchParam:        extraSearchParam,
		PinProductsCsv:          pinProductsCsv,
		SearchRequestId:         searchRequestID,
		IsLuxury:                &_isLuxry,
		ReturnDocs:              &_rDocs,
		IsInlineFilterAvailable: &_isIFA,
		UserQuery:               &_userQuery,
		IsFacet:                 &_isFacet,
		InlineFilterOffset:      ifo,
		InlineFilterCount:       ifc,
		TotalPLAShown:           totalPlashown,
		TotalPLACount:           totalPlacount,
		Id:                      id,
		QueryType:               typeArg,
		PriceBuckets:            priceBuckets,
		RequestType:             requestType,
		SelectedCluster:         selectedCluster,
		ProductsPerCluster:      productsPerCluster,
		ClusterRows:             clusterRows,
	}

	if storefrontID == nil {
		id := helpers.GetStoreFrontId(ctx)
		srp.StorefrontId = &id
	}

	for _, cookie := range ctx.Value("cookies").([]*http.Cookie) {
		if cookie.Name == "spn" {
			*srp.ForcePersonalise = cookie.Value == "true"
			break
		}
	}

	resp, err := search.Post(ctx, srp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (r *searchResolver) PageTitle(ctx context.Context, obj *search.Search) (*string, error) {
	if obj.Seo.MetaData.PageTitle != nil {
		return obj.Seo.MetaData.PageTitle, nil
	}

	return obj.Seo.CanonicalUri, nil
}

func (r *searchProductResolver) CatalogDate(ctx context.Context, obj *search.SearchProduct) (*string, error) {
	catalogDateString := strconv.FormatInt(obj.CatalogDate, 10)
	return &catalogDateString, nil
}
