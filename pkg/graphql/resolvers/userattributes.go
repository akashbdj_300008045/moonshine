package resolvers

// import (
// 	"context"
// 	"strings"

// 	"bitbucket.org/myntra/moonshine/pkg/graphql"
// 	"bitbucket.org/myntra/moonshine/pkg/service/user"
// )

// var _ graphql.UserAttributesResolver = &userAttributeResolver{}

// type userAttributeResolver struct{ *Resolver }

// func (u userAttributeResolver) HasPurchased(ctx context.Context, obj *user.FormattedUser) (*bool, error) {
// 	value := obj.FreeShipping.FirstOrder
// 	if value == nil {
// 		return nil, nil
// 	}

// 	var hasPurchased bool
// 	switch firstOrder := value.(type) {
// 	case bool:
// 		hasPurchased = !firstOrder
// 	case string:
// 		// You might wonder why I need this bizarre check; let me tell you that I do need it, but also that I empathize with you :(
// 		hasPurchased = strings.ToLower(firstOrder) == "false"
// 	default:
// 		return nil, nil
// 	}

// 	return &hasPurchased, nil
// }

// func (u userAttributeResolver) DefaultPincode(ctx context.Context, obj *user.FormattedUser) (*string, error) {
// 	return obj.DefaultAddress.Pincode, nil
// }
