package constants

const (
	NewRelicLicense         = "c9fd78d4facb2a41bc9cf179cd0cca97d0420e04"
	HeaderMyntCtx           = "x-mynt-ctx"
	HeaderUidx              = "m-uidx"
	HeaderClientId          = "m-clientid"
	HeaderXmetaApp          = "x-myntra-app"
	HeaderMetaApp           = "x-meta-app"
	HeaderStoreId           = "m-storeid"
	HeaderUserAgent         = "user-agent"
	SearchAtlasEventHostURL = "www.myntra.com/search/data/"
	HeaderMyntraABTest      = "x-myntra-abtest"
	HeaderMetaABTest        = "x-meta-abtest"
	HeaderPaymentClient     = "client"
	HeaderPaymentVersion    = "version"
	HeaderStyleOffersUidx   = "login"
	HeaderUserState         = "user-state"
)
