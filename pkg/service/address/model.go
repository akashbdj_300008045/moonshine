package address

type Payload struct {
	Id               string    `json:"id,omitempty"`
	UserId           string    `json:"userId,omitempty"`
	IsDefault        *bool     `json:"defaultAddress"`
	AddressType      string    `json:"addressType"`
	StreetAddress    string    `json:"address"`
	Locality         string    `json:"locality"`
	City             string    `json:"city"`
	Pincode          string    `json:"pincode"`
	StateCode        string    `json:"stateCode"`
	StateName        string    `json:"stateName"`
	CountryCode      string    `json:"countryCode"`
	Name             string    `json:"name"`
	Email            string    `json:"email"`
	Mobile           string    `json:"mobile"`
	NotAvailableDays []*string `json:"notAvailableDays"`
}

type Response struct {
	Address []Address `json:"data"`
	Status  Status    `json:"status"`
}

type Status struct {
	StatusType    string `json:"statusType"`
	StatusMessage string `json:"statusMessage"`
	StatusCode    int64  `json:"statusCode"`
	TotalCount    int32  `json:"totalCount"`
}

type Address struct {
	Id               int32     `json:"id"`
	IsDefault        bool      `json:"defaultAddress"`
	Score            string    `json:"address_scoring"`
	AddressType      string    `json:"addressType"`
	NotAvailableDays []*string `json:"notAvailableDays"`
	StreetAddress    string    `json:"address"`
	Locality         string    `json:"locality"`
	City             string    `json:"city"`
	Pincode          string    `json:"pincode"`
	StateCode        string    `json:"stateCode"`
	StateName        string    `json:"stateName"`
	CountryCode      string    `json:"countryCode"`
	CountryName      string    `json:"countryName"`
	Uidx             string    `json:"userId"`
	Name             string    `json:"name"`
	Email            string    `json:"email"`
	Mobile           string    `json:"mobile"`
	Version          int32     `json:"version"`
	Phone            string    `json:"phone"`
	AddressScore     float64   `json:"addressScore"`
	CheckoutAllowed  bool      `json:"checkoutAllowed"`
}
