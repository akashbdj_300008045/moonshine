package address

import (
	"bytes"
	"context"

	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"bitbucket.org/myntra/moonshine/pkg/api"
	"bitbucket.org/myntra/moonshine/pkg/config"
	"bitbucket.org/myntra/moonshine/pkg/util/errors"
	"bitbucket.org/myntra/moonshine/pkg/util/helpers"
	jsoniter "github.com/json-iterator/go"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

type reqOptions struct {
	method string
	url    string
	data   io.Reader
}

func Post(ctx context.Context, p Payload, url string) (*Response, error) {
	data := new(bytes.Buffer)
	e := json.NewEncoder(data).Encode(p)
	if e != nil {
		return nil, e
	}

	resp, err := handleRequest(ctx, reqOptions{method: "POST", url: url, data: data})
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	contents, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var adr *Response
	if err = json.Unmarshal(contents, &adr); err != nil {
		return nil, err
	}

	if adr.Status.StatusType == "ERROR" {
		return nil, errors.CreateError(
			adr.Status.StatusCode,
			adr.Status.StatusMessage,
			http.StatusInternalServerError)
	}

	if len(adr.Address) <= 0 {
		e := errors.CreateError(adr.Status.StatusCode, adr.Status.StatusMessage, http.StatusInternalServerError)
		fmt.Println("[Add Address] no address returned in response", e)
		return nil, e
	}

	return adr, nil
}

func handleRequest(ctx context.Context, options reqOptions) (*http.Response, error) {
	headers := helpers.ReqHeaders(ctx, nil)
	httpClient := api.HTTPClient{}
	resp, err := httpClient.Set(headers).Send(options.data).Timeout(config.GetTimeouts("address")).Do(options.method, options.url)
	if err != nil {
		fmt.Println("=== error in making request to address service ===", err)
		return checkErrors(resp, err)
	}

	return resp, nil
}

// checkErrors handles non-200 errors returned from http.Client
func checkErrors(resp *http.Response, currentError error) (*http.Response, error) {
	if resp == nil {
		return resp, currentError
	}

	var r *Response
	contents, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if err = json.Unmarshal(contents, &r); err != nil {
		return nil, err
	}

	e, _ := currentError.(*errors.CustomError)
	e.Message = r.Status.StatusMessage
	e.Code = r.Status.StatusCode

	return nil, e
}
