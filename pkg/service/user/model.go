package user

// Hallmark service response
type ServiceResponse struct {
	FormattedUser FormattedUser `json:"formattedUser"`
}

type FormattedUser struct {
	DefaultAddress       DefaultAddress          `json:"default_address"`
	FreeShipping         FreeShipping            `json:"free_shipping"`
	ShippedMobileNumbers *map[string]interface{} `json:"shipped_mobile_numbers"`
	MyntraInsider        *map[string]interface{} `json:"myntra_insider"`
}

type DefaultAddress struct {
	Pincode *string `json:"pincode"`
}

type FreeShipping struct {
	FirstOrder interface{} `json:"first_order"`
}
