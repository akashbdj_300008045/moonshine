package search

import "github.com/google/uuid"

type SearchRequestParams struct {
	Query                   string
	Offset                  *int
	Rows                    *int
	InlineFilterOffset      *int
	InlineFilterCount       *int
	TotalPLAShown           *int
	TotalPLACount           *int
	Id                      *int
	PriceBuckets            *int
	ProductsPerCluster      *int
	ClusterRows             *int
	Filters                 *string
	RangeFilters            *string
	Sort                    *string
	StorefrontId            *string
	Pincode                 *string
	GeoF                    *string
	ExtraSearchParam        *string
	PinProductsCsv          *string
	SearchRequestId         *string
	QueryType               *string
	RequestType             *string
	SelectedCluster         *string
	UserQuery               *bool
	ReturnDocs              *bool
	IsFacet                 *bool
	IsLuxury                *bool
	IsInlineFilterAvailable *bool
	ForcePersonalise        *bool
}

type InputSearch struct {
	Query               string                    `json:"query"`
	Start               *int                      `json:"start"`
	Rows                *int                      `json:"rows"`
	Pincode             *string                   `json:"pincode,omitempty"`
	StorefrontId        *string                   `json:"storefrontId,omitempty"`
	AppliedParams       *InputAppliedParams       `json:"appliedParams,omitempty"`
	ExtraSearchParam    *string                   `json:"extra_search_param,omitempty"`
	PinProductsCsv      *string                   `json:"pinProductsCsv,omitempty"`
	ReturnDocs          *bool                     `json:"returnDocs"`
	SearchRequestId     *string                   `json:"searchRequestId,omitempty"`
	TotalPLAShown       *int                      `json:"totalPLAShown,omitempty"`
	TotalPLACount       *int                      `json:"totalPLACount,omitempty"`
	IsFacet             *bool                     `json:"isFacet"`
	IsLuxury            *bool                     `json:"isLuxury"`
	ForcePersonalise    *bool                     `json:"forcePersonalise,omitempty"`
	BehaviouralMetaData *InputBehaviouralMetadata `json:"behaviouralMetaData,omitempty"`
	UserQuery           *bool                     `json:"userQuery"`
	NewUser             *bool                     `json:"newUser,omitempty"`
	ResolvedQuery       *ResolvedQuery            `json:"resolvedQuery,omitempty"`
	PriceBuckets        *int                      `json:"priceBuckets,omitempty"`
	RequestType         *string                   `json:"requestType,omitempty"`
	SelectedCluster     *string                   `json:"selectedCluster,omitempty"`
	ProductsPerCluster  *int                      `json:"productsPerCluster,omitempty"`
	ClusterRows         *int                      `json:"clusterRows,omitempty"`
}

type InputAppliedParams struct {
	Filters      []*InputFilter      `json:"filters,omitempty"`
	RangeFilters []*InputRangeFilter `json:"rangeFilters,omitempty"`
	Sort         *string             `json:"sort,omitempty"`
	GeoFilters   []*InputGeoFilter   `json:"geoFilters,omitempty"`
}

type InputFilter struct {
	Id     string   `json:"id"`
	Values []string `json:"values"`
}

type InputRangeFilter struct {
	Id     string                   `json:"id"`
	Values []*InputRangeFilterValue `json:"values"`
}

type InputRangeFilterValue struct {
	Id    string `json:"id"`
	Start int32  `json:"start"`
	End   int32  `json:"end"`
}

type InputGeoFilter struct {
	InputFilter
}

type InputBehaviouralMetadata struct {
	InlineFilterStatus *InputInlineFilterStatus `json:"inlineFilterStatus,omitempty"`
}

type InputInlineFilterStatus struct {
	IsFilterUnselected *bool `json:"isFilterUnselected,omitempty"`
	InlineFilterOffset *int  `json:"inlineFilterOffset,omitempty"`
	InlineFilterCount  *int  `json:"inlineFilterCount,omitempty"`
}

type ResolvedQuery struct {
	Id   *int    `json:"queryId,omitempty"`
	Type *string `json:"type,omitempty"`
}

type SearchResponse struct {
	Response Search `json:"response"`
}

type StyleResponse struct {
	Data Search `json:"data"`
}

type Search struct {
	SolrQuery             string                 `json:"solrQuery"`
	TotalCount            int32                  `json:"totalCount"`
	TotalPLAShown         int32                  `json:"totalPLAShown"`
	TotalPLACount         int32                  `json:"totalPLACount"`
	HasClusterResults     bool                   `json:"clusterResultAvailable"`
	ResponseType          string                 `json:"responseType"`
	Filters               Filters                `json:"filters"`
	Products              []SearchProduct        `json:"products"`
	SortOptions           []*string              `json:"sortOptions"`
	AppliedParams         AppliedParameters      `json:"appliedParams"`
	GuidedNavigation      GuidedNavigation       `json:"guidedNavigation"`
	NextBestResults       []NextBestResult       `json:"nextBestResults"`
	StorefrontId          string                 `json:"storefrontId"`
	UpsInfo               UpsInfo                `json:"upsinfo"`
	ChangeLog             []ChangeLog            `json:"changeLog"`
	Seo                   SearchSeo              `json:"seo"`
	SortFields            []SearchSortFields     `json:"sortFields"`
	AbTestParams          interface{}            `json:"abTestParams"`
	Clusters              []SearchCluster        `json:"clusters"`
	SelectedClusterValues []SelectedClusterValue `json:"clusterValues"`
	TotalProductCount     int32                  `json:"totalProductCount"`
	QuerySubstitution     *QuerySubstitution     `json:"message"`
	TemplateMessage       *string                `json:"searchMessage"`
}

type Filters struct {
	PrimaryFilters   []Filter       `json:"primaryFilters"`
	SecondaryFilters []Filter       `json:"secondaryFilters"`
	RangeFilters     []RangeFilter  `json:"rangeFilters"`
	GeoFilters       []GeoFilter    `json:"geoFilters"`
	InlineFilters    []InlineFilter `json:"inlineFilters"`
}

type Filter struct {
	Id           string        `json:"id"`
	FilterValues []FilterValue `json:"filterValues"`
	IsVisual     bool          `json:"isVisual"`
}

type FilterValue struct {
	Id     string `json:"id"`
	Count  int32  `json:"count"`
	Meta   string `json:"meta"`
	PLevel string `json:"pLevel"`
	Src    string `json:"src"`
}

type RangeFilter struct {
	Id           string             `json:"id"`
	Gap          float64            `json:"gap"`
	Start        float64            `json:"start"`
	End          float64            `json:"end"`
	FilterValues []RangeFilterValue `json:"filterValues"`
}

type RangeFilterValue struct {
	Id     string  `json:"id"`
	Count  int32   `json:"count"`
	Meta   string  `json:"meta"`
	Start  float64 `json:"start"`
	End    float64 `json:"end"`
	PLevel string  `json:"pLevel"`
}

type GeoFilter struct {
	Id           string           `json:"id"`
	FilterValues []GeoFilterValue `json:"filterValues"`
}

type GeoFilterValue struct {
	MinDays int32  `json:"minDays"`
	MaxDays int32  `json:"maxDays"`
	Num     int32  `json:"num"`
	PLevel  string `json:"pLevel"`
}

type InlineFilter struct {
	Position     PositionInfo        `json:"position"`
	Id           string              `json:"id"`
	FilterValues []InlineFilterValue `json:"filterValues"`
	IsVisual     bool                `json:"isVisual"`
}

type PositionInfo struct {
	AfterProducts int32 `json:"afterProducts"`
}

type InlineFilterValue struct {
	Id     string `json:"id"`
	Count  int32  `json:"count"`
	Meta   string `json:"meta"`
	PLevel string `json:"pLevel"`
	Src    string `json:"src"`
}

type SearchProduct struct {
	Id                                  int32                   `json:"id"`
	Name                                string                  `json:"name"`
	ProductId                           int32                   `json:"productId"`
	Product                             string                  `json:"product"`
	IsPLA                               bool                    `json:"isPLA"`
	AdId                                string                  `json:"adId"`
	Discount                            float64                 `json:"discount"`
	Price                               float64                 `json:"price"`
	Mrp                                 float64                 `json:"mrp"`
	Brand                               string                  `json:"brand"`
	ProductName                         string                  `json:"productName"`
	LandingPageUrl                      string                  `json:"landingPageUrl"`
	LoyaltyPointsEnabled                bool                    `json:"loyaltyPointsEnabled"`
	Rating                              float64                 `json:"rating"`
	RatingCount                         int32                   `json:"ratingCount"`
	IsFastFashion                       bool                    `json:"isFastFashion"`
	SearchImage                         string                  `json:"searchImage"`
	Images                              []Image                 `json:"images"`
	Gender                              string                  `json:"gender"`
	Category                            string                  `json:"category"`
	AdditionalInfo                      string                  `json:"additionalInfo"`
	PrimaryColour                       string                  `json:"primaryColour"`
	DiscountLabel                       string                  `json:"discountLabel"`
	InventoryInfo                       []Inventory             `json:"inventoryInfo"`
	ProductVideos                       []ProductListVideos     `json:"productVideos"`
	Sizes                               string                  `json:"sizes"`
	DiscountDisplayLabel                string                  `json:"discountDisplayLabel"`
	AdvanceOrderTag                     string                  `json:"advanceOrderTag"`
	ColorVariantAvailable               bool                    `json:"colorVariantAvailable"`
	Productimagetag                     string                  `json:"productimagetag"`
	ListViews                           int32                   `json:"listViews"`
	DiscountType                        string                  `json:"discountType"`
	IsPersonalised                      bool                    `json:"isPersonalised"`
	FutureDiscountedPrice               float64                 `json:"futureDiscountedPrice"`
	FutureDiscountStartDate             string                  `json:"futureDiscountStartDate"`
	TdBxGyText                          string                  `json:"tdBxGyText"`
	CatalogDate                         int64                   `json:"catalogDate"`
	Season                              string                  `json:"season"`
	Year                                string                  `json:"year"`
	EorsPicksTag                        string                  `json:"eorsPicksTag"`
	SystemAttributes                    []SystemAttributesEntry `json:"systemAttributes"`
	EffectiveDiscountPercentageAfterTax float64                 `json:"effectiveDiscountPercentageAfterTax"`
	EffectiveDiscountAmountAfterTax     float64                 `json:"effectiveDiscountAmountAfterTax"`
	PersonalizedCoupon                  string                  `json:"personalizedCouponCode"`
	PersonalizedCouponValue             float64                 `json:"personalizedCouponValue"`
	ProductMeta                         string                  `json:"productMeta"`
}

type Image struct {
	View          string `json:"view"`
	Src           string `json:"src"`
	SecureSrc     string `json:"secureSrc"`
	SecuredDomain string `json:"securedDomain"`
	RelativePath  string `json:"relativePath"`
}

type Inventory struct {
	SkuId     int32  `json:"skuid"`
	Label     string `json:"size"`
	Inventory int32  `json:"inventory"`
	Available bool   `json:"available"`
}

type ProductListVideos struct {
	VideoId   string `json:"videoId"`
	VideoHost string `json:"videoHost"`
	VideoUrl  string `json:"videoUrl"`
}

type SystemAttributesEntry struct {
	Attribute string `json:"attribute"`
	Value     string `json:"value"`
}

type AppliedParameters struct {
	Filters      []AppliedFilter    `json:"filters"`
	RangeFilters []InputRangeFilter `json:"rangeFilters"`
	GeoFilters   []AppliedFilter    `json:"geoFilters"`
	Sort         string             `json:"sort"`
}

type AppliedFilter struct {
	Id     string    `json:"id"`
	Values []*string `json:"values"`
}

type GuidedNavigation struct {
	Name             string                  `json:"name"`
	GuidedNavEntries []GuidedNavigationEntry `json:"guidedNavEntries"`
}

type GuidedNavigationEntry struct {
	Key           string `json:"key"`
	Value         string `json:"value"`
	Img           string `json:"img"`
	NumProducts   int32  `json:"numProducts"`
	IsAutocreated bool   `json:"isAutocreated"`
	Rank          int32  `json:"rank"`
}

type NextBestResult struct {
	ListURL            string                  `json:"listURL"`
	DisplayName        string                  `json:"displayName"`
	TotalProductsCount int32                   `json:"totalProductsCount"`
	Products           []NextBestResultProduct `json:"products"`
}

type NextBestResultProduct struct {
	Discount              float64                   `json:"discount"`
	Brand                 string                    `json:"brand"`
	SearchImage           string                    `json:"searchImage"`
	Price                 float64                   `json:"price"`
	Mrp                   float64                   `json:"mrp"`
	Gender                string                    `json:"gender"`
	Sizes                 string                    `json:"sizes"`
	AdditionalInfo        string                    `json:"additionalInfo"`
	ProductName           string                    `json:"productName"`
	Rating                float64                   `json:"rating"`
	RatingCount           int32                     `json:"ratingCount"`
	Product               string                    `json:"product"`
	Category              string                    `json:"category"`
	PrimaryColour         string                    `json:"primaryColour"`
	DiscountLabel         string                    `json:"discountLabel"`
	ProductId             int32                     `json:"productId"`
	DiscountDisplayLabel  string                    `json:"discountDisplayLabel"`
	AllSkuForSizes        []string                  `json:"allSkuForSizes"`
	ImageEntires          []string                  `json:"imageEntires"`
	InventoryInfo         []NBRProductInventoryInfo `json:"inventoryInfo"`
	Images                []NBRProductImage         `json:"images"`
	LandingPageUrl        string                    `json:"landingPageUrl"`
	SystemAttributes      []SystemAttributesEntry   `json:"systemAttributes"`
	FutureDiscountedPrice float64                   `json:"futureDiscountedPrice"`
	ListViews             int32                     `json:"listViews"`
	DiscountType          string                    `json:"discountType"`
	ColorVariantAvailable bool                      `json:"colorVariantAvailable"`
}

type NBRProductInventoryInfo struct {
	Skuid     int32  `json:"skuid"`
	Size      string `json:"size"`
	Inventory int32  `json:"inventory"`
	Available bool   `json:"available"`
}

type NBRProductImage struct {
	View               string `json:"view"`
	Src                string `json:"src"`
	Domain             string `json:"domain"`
	SecuredDomain      string `json:"securedDomain"`
	RelativePath       string `json:"relativePath"`
	ServerUploaderType string `json:"serverUploaderType"`
}

type UpsInfo struct {
	PersonalizationSortAlgoUsed string `json:"personalizationSortAlgoUsed"`
	NumPersonalizedProductShown int32  `json:"numPersonalizedProductShown"`
	IsPersonalized              bool   `json:"isPersonalized"`
}

type SearchSeo struct {
	CanonicalUri *string     `json:"canonicalUri"`
	MetaData     SeoMetaData `json:"metaData"`
}

type SeoMetaData struct {
	PageTitle *string `json:"page_title"`
}

type ChangeLog struct {
	OriginalQuery   string `json:"originalQuery"`
	ChangedQuery    string `json:"changedQuery"`
	SearchComponent string `json:"searchComponent"`
}

type SearchSortFields struct {
	SortField string `json:"sort_field"`
	OderBy    string `json:"order_by"`
}

type SearchCluster struct {
	Id       string `json:"id"`
	Name     string `json:"name"`
	Selected bool   `json:"selected"`
}

type SelectedClusterValue struct {
	Name         string                 `json:"name"`
	TotalCount   int32                  `json:"totalCount"`
	FilterParams map[string]interface{} `json:"filterParams"`
	Products     []SearchProduct        `json:"products"`
}

type QuerySubstitution struct {
	TemplateID     *string        `json:"messageID"`
	InputQuery     *string        `json:"inputQuery"`
	SuggestedQuery *string        `json:"suggestedQuery"`
	DisplayText    *string        `json:"didYouMeanText"`
	ResolvedQuery  *ResolvedQuery `json:"resolvedQuery"`
}

type AtlasPayload struct {
	ClientIP      string           `json:"clientIP"`
	DeviceData    AtlasDeviceData  `json:"deviceData"`
	DeviceID      string           `json:"deviceID"`
	Email         string           `json:"email"`
	HostURL       string           `json:"hostURL"`
	LastPage      bool             `json:"lastPage"`
	Login         string           `json:"login"`
	MabTestData   AtlasMabTestData `json:"mabTestData"`
	Page          string           `json:"page"`
	Quicklook     bool             `json:"quicklook"`
	Referer       AtlasReferer     `json:"referer"`
	RememberLogin string           `json:"rememberLogin"`
	RequestId     *string          `json:"requestId"`
	SearchData    AtlasSearchData  `json:"searchData"`
	ServerName    string           `json:"serverName"`
	Timestamp     int64            `json:"timestamp"`
	Uidx          string           `json:"uidx"`
	UserAgent     string           `json:"userAgent"`
	Uuid          uuid.UUID        `json:"uuid"`
	XMyntraApp    string           `json:"x-myntra-app"`
}

type AtlasDeviceData struct {
	DeviceChannel string             `json:"deviceChannel"`
	DeviceName    string             `json:"deviceName"`
	DeviceType    string             `json:"deviceType"`
	IsDesktop     bool               `json:"isDesktop"`
	IsMobile      bool               `json:"isMobile"`
	IsRobot       bool               `json:"isRobot"`
	IsTablet      bool               `json:"isTablet"`
	MobileDetails AtlasMobileDetails `json:"mobileDetails"`
}

type AtlasMabTestData struct {
	Mabtests []AtlasMabtests `json:"mabtests"`
}

type AtlasMabtests struct {
	AbtestName  string `json:"abtestName"`
	VariantName string `json:"variantName"`
}

type AtlasMobileDetails struct {
	AppName        string `json:"appName"`
	AppVersion     string `json:"appVersion"`
	Device         string `json:"device"`
	InstallationID string `json:"installationID"`
	Resolution     string `json:"resolution"`
	Xid            string `json:"xid"`
}

type AtlasReferer struct {
	Domain string `json:"domain"`
	Host   string `json:"host"`
	Path   string `json:"path"`
}

type AtlasSearchData struct {
	Filters      []string               `json:"filters"`
	NumOfResults int32                  `json:"numOfResults"`
	ResultSet    []AtlasSearchResultset `json:"resultSet"`
	SearchQuery  string                 `json:"searchQuery"`
	Sort         []string               `json:"sort"`
	AbTestParams interface{}            `json:"abTestParams"`
	UpsInfo      bool                   `json:"ups_info"`
	UserQuery    *bool                  `json:"userQuery"`
}

type AtlasSearchResultset struct {
	DiscountedPrice  float64 `json:"discounted_price"`
	DreDiscountLabel string  `json:"dre_discount_label"`
	IsPersonalized   bool    `json:"isPersonalized"`
	Pos              int     `json:"pos"`
	Price            float64 `json:"price"`
	Styleid          int32   `json:"styleid"`
}
