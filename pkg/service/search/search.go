package search

import (
	"bytes"
	"context"
	"io/ioutil"
	"log"
	"strconv"
	"strings"

	"bitbucket.org/myntra/moonshine/pkg/api"
	"bitbucket.org/myntra/moonshine/pkg/config"
	"bitbucket.org/myntra/moonshine/pkg/features"
	"bitbucket.org/myntra/moonshine/pkg/util/helpers"
	jsoniter "github.com/json-iterator/go"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

func Post(
	ctx context.Context,
	params *SearchRequestParams,
) (*Search, error) {

	var (
		f  = parseFilters(params.Filters)
		rf = parseRangeFilters(params.RangeFilters)
		gf = parseGeoFilters(params.GeoF)
	)

	newUser := helpers.GetUserState(ctx) == "NEW_USER"
	rq := &ResolvedQuery{Id: params.Id, Type: params.QueryType}
	bmd := &InputBehaviouralMetadata{
		InlineFilterStatus: &InputInlineFilterStatus{
			IsFilterUnselected: params.IsInlineFilterAvailable,
			InlineFilterOffset: params.InlineFilterOffset,
			InlineFilterCount:  params.InlineFilterCount,
		}}
	ap := &InputAppliedParams{
		Filters:      f,
		RangeFilters: rf,
		Sort:         params.Sort,
		GeoFilters:   gf}

	inputSearch := &InputSearch{
		Query:               params.Query,
		Start:               params.Offset,
		Rows:                params.Rows,
		Pincode:             params.Pincode,
		ReturnDocs:          params.ReturnDocs,
		IsFacet:             params.IsFacet,
		IsLuxury:            params.IsLuxury,
		ForcePersonalise:    params.ForcePersonalise,
		UserQuery:           params.UserQuery,
		StorefrontId:        params.StorefrontId,
		ExtraSearchParam:    params.ExtraSearchParam,
		PinProductsCsv:      params.PinProductsCsv,
		SearchRequestId:     params.SearchRequestId,
		TotalPLAShown:       params.TotalPLAShown,
		TotalPLACount:       params.TotalPLACount,
		PriceBuckets:        params.PriceBuckets,
		NewUser:             &newUser,
		BehaviouralMetaData: bmd,
		AppliedParams:       ap,
		ResolvedQuery:       rq,
		RequestType:         params.RequestType,
		SelectedCluster:     params.SelectedCluster,
		ProductsPerCluster:  params.ProductsPerCluster,
		ClusterRows:         params.ClusterRows,
	}

	reqBytes, _ := json.Marshal(inputSearch)
	body := bytes.NewBuffer(reqBytes)
	headers := helpers.ReqHeaders(ctx, nil)

	httpClient := api.HTTPClient{}
	resp, err := httpClient.Set(headers).Send(body).Timeout(config.GetTimeouts("search")).Post(config.GetUrl("search"))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	contents, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var sr *SearchResponse
	err = json.Unmarshal(contents, &sr)
	if err != nil {
		return nil, err
	}

	cv, err := helpers.GetClientValues(ctx)
	if err == nil {
		if cv["storeid"] == "2297" && features.EnableAtlas {
			go sendServerSideSearchEvents(ctx, sr, params.UserQuery, params.Query, params.SearchRequestId)
		}
	}

	return &sr.Response, nil
}

func parseFilters(fs *string) []*InputFilter {
	iFs := make([]*InputFilter, 0)
	if fs == nil || *fs == "" {
		return iFs
	}

	for _, f := range strings.Split(*fs, "::") {
		fParts := strings.Split(f, ":")
		if len(fParts) != 2 {
			continue
		}
		iF := &InputFilter{
			Id:     fParts[0],
			Values: strings.Split(fParts[1], ","),
		}
		iFs = append(iFs, iF)
	}

	return iFs
}

/*
 * parseRangeFilters parses the given filter string to a format accepted by search service
 *
 * rfs = Price:2_1395_2.0 TO 1395.0,2195_3695_2195.0 TO 3695.0::Discount Range:10_100_10.0,90_100_90.0
 * Upon splitting it at  "::", we get
 * [
 * 	Price: 2_1395_2.0 TO 1395.0, 2195_3695_2195.0 TO 3695.0,
 * 	Discount Range:10_100_10.0,90_100_90.0
 * ]
 */
func parseRangeFilters(rfs *string) []*InputRangeFilter {
	iRFs := make([]*InputRangeFilter, 0)
	if rfs == nil || *rfs == "" {
		return iRFs
	}

	iRFValues := make([]*InputRangeFilterValue, 0)

	for _, rf := range strings.Split(*rfs, "::") {
		fParts := strings.Split(rf, ":")
		if len(fParts) != 2 {
			continue
		}

		filterRanges := strings.Split(fParts[1], ",")
		for _, filterRange := range filterRanges {
			r := strings.Split(filterRange, "_")
			if len(r) != 3 {
				continue
			}

			start, err1 := strconv.ParseFloat(r[0], 32)
			if err1 != nil {
				log.Fatal("Error in converting START value in rangeFilter from STRING to FLOAT", err1)
				continue
			}

			end, err2 := strconv.ParseFloat(r[1], 32)
			if err2 != nil {
				log.Fatal("Error in converting START value in rangeFilter from STRING to FLOAT err2", err2)
				continue
			}

			iRFValue := &InputRangeFilterValue{Id: r[2], Start: int32(start), End: int32(end)}
			iRFValues = append(iRFValues, iRFValue)
		}

		iRF := &InputRangeFilter{Id: fParts[0], Values: iRFValues}
		iRFs = append(iRFs, iRF)
	}

	return iRFs
}

/*
 * GeoFilter query param handling
 * Format is => geoF=delivery_time:id1,id2
 */
func parseGeoFilters(gf *string) []*InputGeoFilter {
	gFs := make([]*InputGeoFilter, 0)
	if gf == nil || *gf == "" {
		return gFs
	}

	_gF := strings.Split(*gf, ":")
	if len(_gF) == 2 {
		iGF := &InputGeoFilter{}
		iGF.Id = _gF[0]
		iGF.Values = strings.Split(_gF[1], ",")
		gFs = append(gFs, iGF)

	}

	return gFs
}
