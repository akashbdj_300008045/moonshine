package search

import (
	"bytes"
	"context"
	"strings"
	"time"

	"bitbucket.org/myntra/moonshine/pkg/api"
	"bitbucket.org/myntra/moonshine/pkg/config"
	"bitbucket.org/myntra/moonshine/pkg/constants"
	"bitbucket.org/myntra/moonshine/pkg/logger"
	"bitbucket.org/myntra/moonshine/pkg/util/helpers"
	"github.com/google/uuid"
	"go.uber.org/zap"
)

func sendServerSideSearchEvents(ctx context.Context, searchResponse *SearchResponse, userQuery *bool, query string, requestId *string) {
	clientIP := ""
	email := ""
	lastPage := false
	uidx := helpers.GetUidx(ctx)
	page := "search"
	quicklook := false
	rememberLogin := ""
	serverName := "dsnew"
	timestamp := time.Now().UnixNano() / 1000000
	userAgent := helpers.GetUserAgent(ctx)
	uuidValue := uuid.New()
	xMyntraApp := helpers.GetXMyntraApp(ctx)

	var deviceID, installationID, appVersion, appFamily, hostURL string
	if xMyntraApp != "" {
		if strings.Contains(xMyntraApp, "deviceID=") {
			deviceID = strings.Split(strings.Split(xMyntraApp, "deviceID=")[1], ";")[0]
		}
		if strings.Contains(xMyntraApp, "installationID=") {
			installationID = strings.Split(strings.Split(xMyntraApp, "installationID=")[1], ";")[0]
		}
		if strings.Contains(xMyntraApp, "appVersion=") {
			appVersion = strings.Split(strings.Split(xMyntraApp, "appVersion=")[1], ";")[0]
		}
		if strings.Contains(xMyntraApp, "appFamily=") {
			appFamily = strings.Split(strings.Split(xMyntraApp, "appFamily=")[1], ";")[0]
		}
	}

	searchQuery := helpers.GetQuery(ctx).(string)
	if searchQuery != "" {
		hostURL = constants.SearchAtlasEventHostURL + query + "?" + searchQuery
	}

	mobileDetails := AtlasMobileDetails{
		AppName:        "",
		AppVersion:     appVersion,
		Device:         "",
		InstallationID: installationID,
		Resolution:     "",
		Xid:            "",
	}

	var deviceData AtlasDeviceData

	if appFamily == "MyntraRetailAndroid" {
		deviceData = AtlasDeviceData{
			DeviceChannel: "mobile_app",
			DeviceName:    "Android MyntraRetailAndroid",
			DeviceType:    "mobile",
			IsDesktop:     false,
			IsMobile:      true,
			IsRobot:       false,
			IsTablet:      false,
			MobileDetails: mobileDetails,
		}
	} else if appFamily == "MyntraRetailiOS" {
		deviceData = AtlasDeviceData{
			DeviceChannel: "mobile_app",
			DeviceName:    "iOS MyntraRetailiOS",
			DeviceType:    "mobile",
			IsDesktop:     false,
			IsMobile:      true,
			IsRobot:       false,
			IsTablet:      false,
			MobileDetails: mobileDetails,
		}
	} else {
		deviceData = AtlasDeviceData{
			DeviceChannel: "",
			DeviceName:    "",
			DeviceType:    "",
			IsDesktop:     true,
			IsMobile:      false,
			IsRobot:       false,
			IsTablet:      false,
			MobileDetails: mobileDetails,
		}

	}

	referer := AtlasReferer{
		Domain: "",
		Host:   "",
		Path:   "",
	}

	mabTestData := AtlasMabTestData{
		Mabtests: make([]AtlasMabtests, 0),
	}

	var resultSet []AtlasSearchResultset

	for i, x := range searchResponse.Response.Products {

		resultSet = append(resultSet, AtlasSearchResultset{
			DiscountedPrice:  x.Price,
			DreDiscountLabel: x.DiscountDisplayLabel,
			IsPersonalized:   x.IsPersonalised,
			Pos:              i + 1,
			Price:            x.Mrp,
			Styleid:          x.ProductId,
		})
	}

	var sort []string

	for _, x := range searchResponse.Response.SortFields {
		sort = append(sort, x.SortField)
	}

	searchData := AtlasSearchData{
		Filters:      make([]string, 0),
		NumOfResults: searchResponse.Response.TotalCount,
		ResultSet:    resultSet,
		SearchQuery:  searchResponse.Response.SolrQuery,
		Sort:         sort,
		AbTestParams: searchResponse.Response.AbTestParams,
		UpsInfo:      searchResponse.Response.UpsInfo.IsPersonalized,
		UserQuery:    userQuery,
	}

	atlasPayload := AtlasPayload{
		ClientIP:      clientIP,
		DeviceData:    deviceData,
		DeviceID:      deviceID,
		Email:         email,
		HostURL:       hostURL,
		LastPage:      lastPage,
		Login:         uidx,
		MabTestData:   mabTestData,
		Page:          page,
		Quicklook:     quicklook,
		Referer:       referer,
		RememberLogin: rememberLogin,
		RequestId:     requestId,
		SearchData:    searchData,
		ServerName:    serverName,
		Timestamp:     timestamp,
		Uidx:          uidx,
		UserAgent:     userAgent,
		Uuid:          uuidValue,
		XMyntraApp:    xMyntraApp,
	}
	atreqBytes, _ := json.Marshal(atlasPayload)
	body := bytes.NewBuffer(atreqBytes)
	headers := helpers.ReqHeaders(ctx, nil)

	// StatD - measure fn removed. TODO: add back!
	httpClient := api.HTTPClient{}
	_, err := httpClient.
		Set(headers).
		Send(body).
		Timeout(config.GetTimeouts("atlas")).
		Post(config.GetUrl("atlas"))
	if err != nil {
		logger.AppLogger.Error(ctx, "Error in making request to Atlas Service", zap.Error(err))
	}

	return
}
