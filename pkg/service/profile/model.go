package profile

import "strconv"

type ProfileResponse struct {
	Entry  UserProfile `json:"entry"`
	Status ApiStatus   `json:"status"`
}

type ApiStatus struct {
	StatusCode    int64  `json:"statusCode"`
	StatusType    string `json:"statusType"`
	StatusMessage string `json:"statusMessage"`
}

type UserProfile struct {
	FirstName       string         `json:"firstName"`
	LastName        string         `json:"lastName"`
	EmailDetails    []EmailDetails `json:"emailDetails"`
	PhoneDetails    []PhoneDetails `json:"phoneDetails"`
	PublicProfileId string         `json:"publicProfileId"`
	UserType        string         `json:"userType"`
	IsActive        string         `json:"status"`
	Gender          string         `json:"gender"`
	Uidx            string         `json:"uidx"`
	Bio             string         `json:"bio"`
	DateOfBirth     int64          `json:"dob"`
	Location        string         `json:"location"`
	RegistrationOn  int64          `json:"registrationOn"`
	IsNewUser       bool           `json:"new_"`
	Image           *string        `json:"image"`
	CoverImage      *string        `json:"coverImage"`
}

type EmailDetails struct {
	Email   string `json:"email"`
	Primary bool   `json:"primary"`
}

type PhoneDetails struct {
	Phone    string `json:"phone"`
	Primary  bool   `json:"primary,omitempty"`
	Verified bool   `json:"verfied,omitempty"`
}

type ProfileName struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

type ProfileContact struct {
	Email string `json:"email"`
	Phone string `json:"phone"`
}

type ProfileImages struct {
	ProfileImage *string
	CoverImage   *string
}

type Insider struct {
	TierName      string `json:"tierName"`
	Status        string `json:"status"`
	LoyaltyPoints int32  `json:"loyaltyPoints"`
}

// ----------------- Resolver methods for unmatched keys b/w Graphql schema and Go struct ------------------
// TODO: Discuss with nishank whether to generate the interface or manually write them like this.
// Also, should it be moved to resolvers file?

func (p UserProfile) Name() *ProfileName {
	return &ProfileName{
		FirstName: p.FirstName,
		LastName:  p.LastName,
	}
}

func (p UserProfile) Contact() *ProfileContact {
	primaryContact := getPrimaryContact(p.PhoneDetails, p.EmailDetails)
	return &ProfileContact{
		Phone: primaryContact.Phone,
		Email: primaryContact.Email,
	}
}

func (p UserProfile) IsPhoneVerified() bool {
	var verified bool
	for _, val := range p.PhoneDetails {
		if val.Verified == true {
			verified = true
			break
		}
	}
	return verified
}

func (p UserProfile) Dob() string {
	return strconv.Itoa(int(p.DateOfBirth))
}

func (p UserProfile) Images() *ProfileImages {
	return &ProfileImages{
		ProfileImage: p.Image,
		CoverImage:   p.CoverImage,
	}
}

func getPrimaryContact(pd []PhoneDetails, ed []EmailDetails) ProfileContact {
	var primaryContact ProfileContact
	for _, val := range pd {
		if val.Primary == true {
			primaryContact.Phone = val.Phone
			break
		}
	}
	for _, val := range ed {
		if val.Primary == true {
			primaryContact.Email = val.Email
			break
		}
	}

	return primaryContact
}
