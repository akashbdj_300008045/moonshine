// Code generated by github.com/99designs/gqlgen, DO NOT EDIT.

package models

type AddressInput struct {
	ID               *string       `json:"id"`
	IsDefault        *bool         `json:"isDefault"`
	AddressType      string        `json:"addressType"`
	Name             string        `json:"name"`
	Email            string        `json:"email"`
	Mobile           string        `json:"mobile"`
	StreetAddress    string        `json:"streetAddress"`
	Locality         string        `json:"locality"`
	City             string        `json:"city"`
	Pincode          string        `json:"pincode"`
	State            *StateInput   `json:"state"`
	Country          *CountryInput `json:"country"`
	NotAvailableDays []*string     `json:"notAvailableDays"`
}

type Country struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

type CountryInput struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

type State struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

type StateInput struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

type User struct {
	Uidx   string `json:"uidx"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Mobile string `json:"mobile"`
}
