package config

import (
	"io"
	"io/ioutil"

	jsoniter "github.com/json-iterator/go"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

type CfgSelect struct {
	Production  *Cfg `json:"production"`
	Development *Cfg `json:"development"`
	Testing     *Cfg `json:"testing"`
	Dockins     *Cfg `json:"dockins"`
}

type Cfg struct {
	Name            string            `json:"name"`
	ServiceUrls     map[string]string `json:"service.urls"`
	ServiceKey      map[string]string `json:"service.key"`
	ServiceClient   map[string]string `json:"service.client"`
	ServiceVersion  map[string]string `json:"service.version"`
	ServiceTimeouts map[string]int    `json:"service.timeouts"`
}

var selectedCfg *Cfg
var testEndpoints = make(map[string]string)

func Load(cfgFile io.Reader, env string) error {
	raw, err := ioutil.ReadAll(cfgFile)
	if err != nil {
		return err
	}

	var cfgSelect *CfgSelect
	if err = json.Unmarshal(raw, &cfgSelect); err != nil {
		return err
	}

	if env == "production" {
		selectedCfg = cfgSelect.Production
	} else if env == "development" {
		selectedCfg = cfgSelect.Development
	} else if env == "testing" {
		selectedCfg = cfgSelect.Testing
	} else if env == "dockins" {
		selectedCfg = cfgSelect.Dockins
	}

	return nil
}

func GetUrl(svc string) string {
	if selectedCfg.Name != "testing" {
		return selectedCfg.ServiceUrls[svc]
	}
	if selectedCfg.Name == "testing" && testEndpoints[svc] != "" {
		return testEndpoints[svc]
	}
	return selectedCfg.ServiceUrls[svc]
}

func GetClient(svc string) string {
	return selectedCfg.ServiceClient[svc]
}

func GetVersion(svc string) string {
	return selectedCfg.ServiceVersion[svc]
}

func GetSecretKey(svc string) string {
	return selectedCfg.ServiceKey[svc]
}

func SetUrlForTests(svc, url string) {
	if selectedCfg.Name == "testing" {
		testEndpoints[svc] = url
	}
}

func GetTimeouts(svc string) int {
	return selectedCfg.ServiceTimeouts[svc]
}
