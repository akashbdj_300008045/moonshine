package log

import (
	"io"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

const SEP = string(filepath.Separator)

var infoLogger *log.Logger
var errLogger *log.Logger
var RequestLogger *log.Logger

func Init(lpath string, env string) {
	var infoPath = lpath + SEP + "info.log"
	var errPath = lpath + SEP + "err.log"
	var requestPath = lpath + SEP + "request.log"

	infoLogger = log.New()
	errLogger = log.New()
	RequestLogger = log.New()

	if env == "production" {
		if _, err := os.Stat(lpath); os.IsNotExist(err) {
			os.Mkdir(lpath, 0755)
		}
		infoLogger.Formatter = &log.JSONFormatter{}
		infoLogger.Out = writer(infoPath)
		errLogger.Formatter = &log.JSONFormatter{}
		errLogger.Out = writer(errPath)
		RequestLogger.Formatter = &log.JSONFormatter{}
		RequestLogger.Out = writer(requestPath)
	}
}

func writer(path string) io.Writer {
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	return f
}

func Info(args ...interface{}) {
	infoLogger.Infoln(args...)
}

func Err(args ...interface{}) {
	errLogger.Errorln(args...)
}
