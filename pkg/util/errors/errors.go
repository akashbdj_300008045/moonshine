package errors

type CustomError struct {
	Code       int64  `json:"code"`
	Message    string `json:"message"`
	HttpStatus int    `json:"-"`
}

func (c CustomError) Error() string {
	return c.Message
}

const (
	ErrorUnknown               = "ERR_UNKNOWN"
	ErrorInvalidInput          = "ERR_INPUT_DECODE"
	ErrorServiceConnection     = "ERR_SERVICE_CONNECTION"
	ErrorUnauthorized          = "ERR_UNAUTHOURIZED"
	ErrorForbidden             = "ERR_FORBIDDEN"
	ErrorNotFound              = "ERR_NOT_FOUND"
	ErrorCodeInvalidInput      = 1001
	ErrorCodeUnknown           = 1002
	ErrorCodeServiceConnection = 1003
	ErrorCodeUnauthorized      = 1004
	ErrorCodeForbidden         = 1005
	ErrorCodeNotFound          = 1006
	ErrorCodeCustomMessage     = 1007
)

var ErrorCodeMap = map[int]string{
	500: ErrorUnknown,
	400: ErrorInvalidInput,
	401: ErrorUnauthorized,
	403: ErrorForbidden,
	404: ErrorNotFound}

var errorsMap map[string]*CustomError

func init() {
	errorsMap = make(map[string]*CustomError)
	errorsMap[ErrorInvalidInput] = &CustomError{
		Code:       ErrorCodeInvalidInput,
		Message:    "Invalid input",
		HttpStatus: 400,
	}
	errorsMap[ErrorUnknown] = &CustomError{
		Code:       ErrorCodeUnknown,
		Message:    "Unknown error",
		HttpStatus: 500,
	}
	errorsMap[ErrorServiceConnection] = &CustomError{
		Code:       ErrorCodeServiceConnection,
		Message:    "Cannot connect to the service",
		HttpStatus: 500,
	}
	errorsMap[ErrorUnauthorized] = &CustomError{
		Code:       ErrorCodeUnauthorized,
		Message:    "Unauthorized Request",
		HttpStatus: 401,
	}
	errorsMap[ErrorForbidden] = &CustomError{
		Code:       ErrorCodeForbidden,
		Message:    "Forbidden Request",
		HttpStatus: 403,
	}
	errorsMap[ErrorNotFound] = &CustomError{
		Code:       ErrorCodeNotFound,
		Message:    "Not Found",
		HttpStatus: 404,
	}
}

func GetError(code string) *CustomError {
	if errorsMap[code] == nil {
		return errorsMap[ErrorUnknown]
	}
	return errorsMap[code]
}

func CreateError(code int64, message string, httpStatus int) *CustomError {
	return &CustomError{
		Code:       code,
		Message:    message,
		HttpStatus: httpStatus,
	}
}
