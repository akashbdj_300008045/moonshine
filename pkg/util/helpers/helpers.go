package helpers

import (
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"

	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"

	jsoniter "github.com/json-iterator/go"

	"sort"
	"strconv"

	"bitbucket.org/myntra/moonshine/pkg/config"
	"bitbucket.org/myntra/moonshine/pkg/constants"

	"bitbucket.org/myntra/moonshine/pkg/api"
	"bitbucket.org/myntra/moonshine/pkg/util/errors"
	"github.com/nytlabs/gojsonexplode"

	lz4 "github.com/bkaradzic/go-lz4"
	"go.uber.org/zap"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

type ReqOptions struct {
	Method  string
	Url     string
	Payload io.Reader
}

func ReqHeaders(ctx context.Context, additonalHeaders map[string]string) http.Header {
	reqHeaders := http.Header{}
	headers, ok := ctx.Value("headers").(map[string]string)
	if !ok {
		return reqHeaders
	}

	for k, v := range headers {
		reqHeaders.Add(k, v)
	}
	for k, v := range additonalHeaders {
		reqHeaders.Add(k, v)
	}
	reqHeaders.Add("Accept", "application/json")
	reqHeaders.Add("Content-Type", "application/json")
	return reqHeaders
}

func CompressAbTest(abTest string) string {
	if abTest == "" {
		return ""
	}
	if len(abTest) < 1500 {
		return "0:" + abTest
	}
	toCompress := []byte(abTest)
	compressed := make([]byte, len(toCompress))
	//compress
	compressed, err := lz4.Encode(nil, toCompress)
	if err != nil {
		log.Fatal("There was an error in doing lz4 compression", zap.Error(err))
		return "0:" + abTest
	}
	/*
		First 4 bytes(uint32) of the compressed array stores length of the input
		To make it work with various language we are removing the first 4 bytes
	*/
	/*
		When decompressing in golang append []byte{16, 39, 0, 0} (10000 bytes) before the compressed byte array
		once decomressed trim the byte array bytes.Trim(uncompressed, string(0))

		compressed := url.PathUnescape(abHeader[2:])
		decompressed := make([]byte, 10000)
		output = append([]byte{16, 39, 0, 0}, []byte(compressed)...)
		decompressed, err = lz4.Decode(nil, output)
		abTest = string(bytes.Trim(decompressed, string(0))
	*/
	return "1:" + url.PathEscape(string(compressed[4:]))
}

func GetUidx(ctx context.Context) string {
	user, ok := ctx.Value("user").(map[string]string)
	if !ok {
		return ""
	}

	if user == nil {
		return ""
	}

	return user[constants.HeaderUidx]
}

func GetUserState(ctx context.Context) string {
	user, ok := ctx.Value("user").(map[string]string)
	if !ok {
		return ""
	}

	if user == nil {
		return ""
	}
	return user[constants.HeaderUserState]
}

func GetClientId(ctx context.Context) string {
	headers, ok := ctx.Value("headers").(map[string]string)
	if !ok {
		return ""
	}

	if headers == nil {
		return ""
	}
	return headers[constants.HeaderClientId]
}

func GetXMyntraApp(ctx context.Context) string {
	headers, ok := ctx.Value("headers").(map[string]string)
	if !ok {
		return ""
	}

	if headers == nil {
		return ""
	}
	return headers[constants.HeaderXmetaApp]
}

func GetMetaABTest(ctx context.Context) string {
	headers, ok := ctx.Value("headers").(map[string]string)
	if !ok {
		return ""
	}

	if headers == nil {
		return ""
	}
	return headers[constants.HeaderMetaABTest]
}

func GetXMyntCtx(ctx context.Context) string {
	headers, ok := ctx.Value("headers").(map[string]string)
	if !ok {
		return ""
	}

	if headers == nil {
		return ""
	}
	return headers[constants.HeaderMyntCtx]
}

func GetStoreId(ctx context.Context) string {
	headers, ok := ctx.Value("headers").(map[string]string)
	if !ok {
		return ""
	}

	if headers == nil {
		return ""
	}
	return headers[constants.HeaderStoreId]
}

func GetMyntraABTest(ctx context.Context) string {
	headers, ok := ctx.Value("headers").(map[string]string)
	if !ok {
		return ""
	}

	if headers == nil {
		return ""
	}
	return headers[constants.HeaderMyntraABTest]
}

func GetUserAgent(ctx context.Context) string {
	headers, ok := ctx.Value("headers").(map[string]string)
	if !ok {
		return ""
	}

	if headers == nil {
		return ""
	}
	return headers[constants.HeaderUserAgent]
}

func GetQuery(ctx context.Context) interface{} {
	headers := ctx.Value("query")
	if headers == nil {
		return ""
	}
	return headers
}

func FloatToIntArray(floatArr []float64) []int {
	var intArray []int
	for _, f := range floatArr {
		i := int(f)
		intArray = append(intArray, i)
	}
	return intArray
}

func IntArray64To32(int64Array []int64) []int32 {
	var int32Array []int32
	for _, element := range int64Array {
		int32Array = append(int32Array, int32(element))
	}
	return int32Array
}

// GetClientValues : Deciphering the headers for the client values.
func GetClientValues(ctx context.Context) (map[string]string, error) {
	var values []string
	clientValues := make(map[string]string)

	headers, ok := ctx.Value("headers").(map[string]string)
	if !ok {
		return clientValues, nil
	}

	myntraCtxHeader := headers[constants.HeaderMyntCtx]
	if myntraCtxHeader == "" {
		return nil, &errors.CustomError{
			HttpStatus: 500,
			Code:       500,
			Message:    "APIfy: x-mynt-ctx not present in the request",
		}
	}

	values = strings.Split(myntraCtxHeader, ";")

	for _, pair := range values {
		if pair != "" {
			value := strings.Split(pair, "=")
			clientValues[value[0]] = value[1]
		}
	}
	return clientValues, nil
}

// GenerateRequest : Function to generate requests for SCM systems.
func GenerateRequest(ctx context.Context, options ReqOptions) (*http.Request, error) {
	req, err := http.NewRequest(options.Method, options.Url, options.Payload)
	if err != nil {
		return nil, err
	}

	// Headers needed by all of the SCM of the APIs.
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Authorization", "Basic U3lzdGVtflN5c3RlbTpTeXN0ZW0=")
	if ctx != nil {
		api.SetHeaders(req, ctx)
	}

	return req, nil
}

// GenerateSFRequest : Function to generate requests for SF systems.
func GenerateSFRequest(ctx context.Context, options ReqOptions) (*http.Request, error) {
	req, err := http.NewRequest(options.Method, options.Url, options.Payload)
	if err != nil {
		return nil, err
	}

	// Headers for all the SF Systems.
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	if ctx != nil {
		api.SetHeaders(req, ctx)
	}

	return req, nil
}

func ArrayToMap(elements []int) map[int]int {
	elementMap := make(map[int]int)
	for i := 0; i < len(elements); i++ {
		elementMap[elements[i]] = i
	}
	return elementMap
}

// CreateSignature : Generation of Authorization Logic for Payments Eco System Service API's
func CreateSignature(jsonRequestString string, application string) (string, error) {
	// Generate Signature of the payload
	// convert to JSON
	client := config.GetClient(application)
	version := config.GetVersion(application)
	secretKey := config.GetSecretKey(application)

	sha := sha256.New()
	authorization, err := GenerateAuthorization(jsonRequestString, client, version, secretKey)
	if err != nil {
		log.Fatal(
			"There was a problem in Generating Authorization",
			zap.Error(err),
			zap.String("jsonRequestString", jsonRequestString),
			zap.String("client", client),
			zap.String("version", version),
			zap.String("secretKey", secretKey),
		)
		return "", nil
	}

	sha.Write([]byte(authorization))
	return hex.EncodeToString(sha.Sum(nil)), nil
}

// GenerateAuthorization : Helper Method, to generate the signature.
func GenerateAuthorization(requestJsonString string, client string, version string, secretKey string) (string, error) {

	// Flattens our JSON object
	out, err := gojsonexplode.Explodejsonstr(requestJsonString, ".")
	if err != nil {
		log.Fatal(
			"There was an error in gojsonexplode - Flattening json",
			zap.Error(err),
			zap.String("requestJsonString", requestJsonString),
		)
	}

	// convert our flattened JSON to map
	n := []byte(out)
	var ma map[string]interface{}
	if err := json.Unmarshal(n, &ma); err != nil {
		log.Fatal(
			"There was an error in unmarshalling - while converting json to map",
			zap.Error(err),
		)
		return "", err
	}

	// extract out all keys of map to array
	var arr []string
	for key := range ma {
		arr = append(arr, key)
	}

	// Sort our array of keys
	sort.Strings(arr)

	// For sorted keys in array, get their corresponding values
	// from map
	auth := []string{client, version}
	for _, v := range arr {
		if ma[v] != nil {
			s := getString(ma[v])
			if len(s) > 0 {
				auth = append(auth, s)
			}
		}
	}
	auth = append(auth, secretKey)

	joined := strings.Join(auth, "|")
	return joined, nil
}

func getString(val interface{}) string {
	switch concreteVal := val.(type) {
	case float64:
		return strconv.Itoa(int(concreteVal))
	default:
		return concreteVal.(string)
	}
}

func GenerateRandomNumber(upperLimit int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(upperLimit)
}

func HandleRequest(ctx context.Context, method string, serviceName string, urlpath string, input interface{}, headers map[string]string, response interface{}) error {
	httpClient := &api.HTTPClient{}
	body := new(bytes.Buffer)
	json.NewEncoder(body).Encode(input)
	log.Fatal("HandleRequest : Input body ", zap.String("body", string(body.Bytes())))

	url := config.GetUrl(serviceName) + urlpath
	httpClient = httpClient.Set(ReqHeaders(ctx, headers)).Send(body).Timeout(config.GetTimeouts(serviceName))
	var resp *http.Response
	var err error
	switch method {
	case "GET":
		resp, err = httpClient.Get(url)
		break
	case "POST":
		resp, err = httpClient.Post(url)
		break
	case "PUT":
		resp, err = httpClient.Put(url)
		break
	}
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	contents, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(contents, response)
	if err != nil {
		return err
	}

	return nil
}

func Contains(x []string, y string) bool {
	for _, n := range x {
		if y == n {
			return true
		}
	}
	return false
}

func BasicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

/*
	We have 3 scenarios for this:
		1. If the storefrontId(test1) is present in query params - use that!
		2. If not in query params, check "mab" cookie and get value of the bucket -> use it to find storefrontId in bucket map
		3. If none of the above, use random number between 0 - 100 -> use that to find storeFrontId in
*/
func GetStoreFrontId(ctx context.Context) string {
	var storefrontId string
	cookies := ctx.Value("cookies")
	storefrontId = getStoreFrontIdFromCookies(cookies.([]*http.Cookie))
	if storefrontId == "" {
		bucketKey := GenerateRandomNumber(100)
		storefrontId = getBucketValueForKey(bucketKey)
	}

	return storefrontId
}

func getBucketValueForKey(key int) string {
	// Bucket map => THIS IS HARD-CODED! Might have to change - @nishank.gupta
	bucketKeys := []int{3, 7, 11, 15, 19, 23, 27, 31, 34, 37, 40, 44, 47, 50, 53, 56, 59, 62, 65, 68, 71, 75, 79, 83, 87, 91, 95, 99}
	bucketValues := []string{"test1", "test10", "test11", "test12", "test13", "test14", "test15", "test16", "test17", "test18", "test19", "test2", "test20", "test21", "test22", "test23", "test24", "test25", "test26", "test27", "test28", "test3", "test4", "test5", "test6", "test7", "test8", "test9"}
	for i, val := range bucketKeys {
		if key <= val {
			return bucketValues[i]
		}
	}
	return ""
}

func getStoreFrontIdFromCookies(cookies []*http.Cookie) string {
	var mabCookie string
	for _, cookie := range cookies {
		if cookie.Name == "mab" {
			mabCookie = cookie.Value
			break
		}
	}

	if mabCookie == "" {
		return ""
	}

	mabCookie, err := url.QueryUnescape(mabCookie)
	if err != nil {
		return ""
	}

	// mabCookie will look something like this:
	// checkout|69||styleRevenueAdjusted6|59||NPSRatio|24||cart-mysql-shard|90||serviceabilty-php-call|21
	splittedMab := strings.Split(mabCookie, "||")

	var bucketKey string
	for _, val := range splittedMab {
		if strings.Contains(val, "styleRevenueAdjusted6") {
			bucks := strings.Split(val, "|")
			if len(bucks) == 2 {
				bucketKey = bucks[1]
				break
			}
		}
	}

	// This assumes styleRevenueAdjusted6 key's value will always be of number type
	intBucketKey, err := strconv.Atoi(bucketKey)
	if err != nil {
		return ""
	}

	return getBucketValueForKey(intBucketKey)
}

func PrettyPrint(body []byte) bytes.Buffer {
	var prettyJSON bytes.Buffer
	// error := json.Indent(&prettyJSON, body, "", "\t")
	// if error != nil {
	// 	fmt.Println("can't print pretty json", error)
	// }

	return prettyJSON

}
