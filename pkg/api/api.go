package api

import (
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/myntra/moonshine/pkg/util/errors"
	jsoniter "github.com/json-iterator/go"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

// HTTPClient is the client used to make requests from gateway to other internal services
type HTTPClient struct {
	headers http.Header
	body    io.Reader
	time    int
}

// Set sets headers to the client for a request
func (client *HTTPClient) Set(headers http.Header) *HTTPClient {
	if len(client.headers) == 0 {
		client.headers = make(map[string][]string)
	}
	for k, v := range headers {
		client.headers[k] = v
	}
	return client
}

// Send sets the body to be sent with the request
func (client *HTTPClient) Send(body io.Reader) *HTTPClient {
	client.body = body
	return client
}

// Timeout sets the timeout for the response in seconds
func (client *HTTPClient) Timeout(timeout int) *HTTPClient {
	client.time = timeout
	return client
}

// Get makes a get request
func (client *HTTPClient) Get(path string) (*http.Response, error) {
	return checkErrors(client.Do("GET", path))
}

// Post makes a post request
func (client *HTTPClient) Post(path string) (*http.Response, error) {
	return checkErrors(client.Do("POST", path))
}

// Put makes a put request
func (client *HTTPClient) Put(path string) (*http.Response, error) {
	return checkErrors(client.Do("PUT", path))
}

// Do makes a request of mentioned method
func (client *HTTPClient) Do(method string, path string) (*http.Response, error) {
	//agent with cookies to make request
	agent := &http.Client{
		Timeout: time.Duration(client.time) * time.Second,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	//populate request
	req, requestInitError := http.NewRequest(method, path, client.body)
	if requestInitError != nil {
		return nil, requestInitError
	}
	//set headers
	for k, v := range client.headers {
		if strings.ToLower(k) != "connection" {
			req.Header[k] = v
		}
	}
	//make request
	res, err := checkErrors(agent.Do(req))
	return res, err
}

//Checking errors (non 2xx) received in response from client
func checkErrors(resp *http.Response, err error) (*http.Response, error) {
	// Need to check if the response statuscode does not lie between 200 - 299
	// then send the same error code to the client.
	if resp != nil && (resp.StatusCode < 200 || resp.StatusCode > 299) {
		return resp, errors.GetError(errors.ErrorCodeMap[resp.StatusCode])
	}
	return resp, err
}

var netTransport = &http.Transport{
	Dial: (&net.Dialer{
		Timeout: 5 * time.Second,
	}).Dial,
	TLSHandshakeTimeout: 5 * time.Second,
}

var Client = &http.Client{
	Timeout:   time.Second * 30,
	Transport: netTransport,
}

func SetHeaders(req *http.Request, ctx context.Context) {
	headers := ctx.Value("headers").(map[string]string)
	for k, v := range headers {
		req.Header.Set(k, v)
	}
}

func Do(req *http.Request) (*http.Response, error) {
	resp, err := Client.Do(req)

	// Need to check if the response statuscode does not lie between 200 - 299
	// then send the same error code to the client.
	if resp != nil && (resp.StatusCode < 200 || resp.StatusCode > 299) {
		return resp, errors.GetError(errors.ErrorCodeMap[resp.StatusCode])
	}

	return resp, err
}

func createRequest(ctx context.Context, method string, url string, input interface{}, headers map[string]string) (*http.Request, error) {
	data := new(bytes.Buffer)
	json.NewEncoder(data).Encode(input)

	req, err := http.NewRequest(method, url, data)
	if err != nil {
		return nil, err
	}

	SetHeaders(req, ctx)
	if method != "GET" {
		req.Header.Add("Content-Type", "application/json")
	}
	req.Header.Add("Accept", "application/json")
	// By default http module will send gzip header and uncompress accordingly,
	// In doubt check the response.Uncompressed

	for k, v := range headers {
		req.Header.Add(k, v)
	}

	return req, nil
}

func makeRequest(req *http.Request, res interface{}) error {
	resp, err := Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	contents, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(contents, res)
	if err != nil {
		return err
	}
	return nil
}

func HandleRequest(ctx context.Context, method string, url string, input interface{}, headers map[string]string, response interface{}) error {
	req, err := createRequest(ctx, method, url, input, headers)
	if err != nil {
		return err
	}
	return makeRequest(req, response)
}
