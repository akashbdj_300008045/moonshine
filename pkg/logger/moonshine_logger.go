package logger

import (
	"sync"

	logger "bitbucket.org/myntra/go-common-logger"
)

const (
	ReqId   = "req"
	StoreId = "storeid"
	Uidx    = "uidx"
	Nidx    = "nidx"
	Time    = "time"
	Server  = "server"
)

var AppLogger *logger.Logger
var once sync.Once

func Init(config logger.Config) {
	once.Do(func() {
		config.Level = logger.Info
		config.Keys = []string{ReqId, StoreId, Uidx, Nidx, Time, Server}
		AppLogger = logger.NewLogger(config)
	})
}
