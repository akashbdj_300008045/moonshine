package middleware

import (
	"context"
	"net/http"

	"bitbucket.org/myntra/moonshine/pkg/constants"
)

func MiddlewareUser(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		user := make(map[string]string)
		fill(req, []string{
			constants.HeaderUidx,
			constants.HeaderUserState,
		}, user)

		ctx := context.WithValue(req.Context(), "user", user)
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}
