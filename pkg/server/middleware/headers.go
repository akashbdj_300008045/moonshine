package middleware

import (
	"context"
	"net/http"

	"bitbucket.org/myntra/moonshine/pkg/constants"
)

type MiddlewareFunc func(next http.Handler) http.Handler

func MiddlewareHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		cookies := req.Cookies()
		headers := make(map[string]string)
		// suggest better fn name
		fill(req, []string{
			constants.HeaderMyntCtx,
			constants.HeaderClientId,
			constants.HeaderStoreId,
			constants.HeaderXmetaApp,
			constants.HeaderUserAgent,
			constants.HeaderMetaApp,
		}, headers)

		ctx := context.WithValue(req.Context(), "headers", headers)
		ctx = context.WithValue(ctx, "query", req.URL.RawQuery)
		ctx = context.WithValue(ctx, "cookies", cookies)

		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func fill(req *http.Request, items []string, container map[string]string) {
	for _, h := range items {
		val := req.Header.Get(h)
		if len(val) > 0 {
			container[h] = val
		}
	}
}
