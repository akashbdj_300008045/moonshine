package middleware

import (
	"context"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/go-chi/chi/middleware"
	"github.com/google/uuid"
	cookieParser "github.com/yogeshpandey/gocookie-string-reader"
)

var hostname string

func init() {
	hostname, _ = os.Hostname()
}

const x_mynt_ctx = "x-mynt-ctx"

func MiddlewareCommon(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		xMyntHeader := req.Header.Get(x_mynt_ctx)
		ctx := req.Context()
		if xMyntHeader != "" {
			cookies := cookieParser.ParseToCookie(xMyntHeader)
			for _, c := range cookies {
				ctx = context.WithValue(ctx, c.Name, c.Value)
			}
		}

		if ctx.Value("req") == nil || ctx.Value("req") == "" {
			reqId, _ := uuid.NewRandom()
			ctx = context.WithValue(ctx, "req", hostname+":"+reqId.String()+":"+strconv.Itoa(int(time.Now().Unix())))
		}

		ctx = context.WithValue(ctx, middleware.RequestIDKey, ctx.Value("req"))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}
