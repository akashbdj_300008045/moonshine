package server

import (
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

var currentDir = ""
var liveFile = "/.live"

func checkLocalhost(req *http.Request) bool {
	localhost := []string{"127.0.0.1", "::1", "::ffff:127.0.0.1"}
	fmt.Println("Remote address ", req.RemoteAddr)
	for _, hostname := range localhost {
		if strings.Index(req.RemoteAddr, hostname) > -1 {
			return true
		}
	}
	return false
}

func isLive() bool {
	if _, err := os.Stat(currentDir + liveFile); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func createLive() {
	f, err := os.Create(currentDir + liveFile)
	if err != nil {
		panic(err)
	}
	defer f.Close()
}

func statusRoutes(router chi.Router) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic(err)
	}
	currentDir = dir

	router.Get("/status/bringIntoLB", func(w http.ResponseWriter, r *http.Request) {
		if !checkLocalhost(r) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if isLive() {
			w.WriteHeader(http.StatusOK)
			return
		}
		w.WriteHeader(http.StatusOK)
		createLive()

	})
	router.Get("/status/bringOutOfLB", func(w http.ResponseWriter, r *http.Request) {
		if !checkLocalhost(r) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if !isLive() {
			w.WriteHeader(http.StatusOK)
			return
		}
		//delete the file
		var err = os.Remove(currentDir + liveFile)
		w.WriteHeader(http.StatusOK)
		if err != nil {
			panic(err)
		}

	})

	router.Get("/status/check", func(w http.ResponseWriter, r *http.Request) {
		if isLive() {
			w.WriteHeader(http.StatusOK)
			return
		}
		w.WriteHeader(http.StatusNotFound)
		return
	})

}
