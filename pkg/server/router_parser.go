package server

import (
	"fmt"
	"io/ioutil"
	"os"
)

type Parameter struct {
	Name      string      `json:"name"`
	Type      string      `json:"type"`
	Mandatory bool        `json:"mandatory"`
	Default   interface{} `json:"default,omitempty"`
	ParamType string
}

type Parameters struct {
	Url     []Parameter `json:"url,omitempty"`
	Headers []Parameter `json:"headers,omitempty"`
	Query   []Parameter `json:"query,omitempty"`
}

type Action struct {
	Type  string      `json:"type"`
	Value interface{} `json:"value"`
}

type Route struct {
	Method     string     `json:"method"`
	Path       string     `json:"path"`
	Parameters Parameters `json:"parameters"`
	Action     Action     `json:"action"`
	Exceptions []string   `json:"exceptions,omitempty"`
}

type Routes struct {
	Routes []Route `json:"routes"`
}

func readConfig(routerConfig string) Routes {
	raw, err := ioutil.ReadFile(routerConfig)
	if err != nil {
		fmt.Println("Unable to read Router Config file")
		os.Exit(1)
	}
	var routes Routes
	marshErr := json.Unmarshal(raw, &routes)
	if marshErr != nil {
		fmt.Println("Unable to read Router Config file")
		os.Exit(1)
	}
	return routes
}
