package server

import (
	"errors"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
)

func getVariables(req *http.Request, route Route) (error, map[string]interface{}) {
	var variables = make(map[string]interface{})
	var err error
	err = getParams(req, variables, route.Parameters.Url, "url")
	if err != nil {
		return err, nil
	}
	err = getParams(req, variables, route.Parameters.Headers, "header")
	if err != nil {
		return err, nil
	}
	err = getParams(req, variables, route.Parameters.Query, "query")
	if err != nil {
		return err, nil
	}

	contentType := strings.ToLower(req.Header.Get("content-type"))
	if !strings.HasPrefix(contentType, "application/json") {
		return nil, variables
	}

	var bJson interface{}
	err = json.NewDecoder(req.Body).Decode(&bJson)
	if err != nil {
		return err, nil
	}

	if bJson == nil {
		return nil, variables
	}

	variables["req_body"] = bJson
	return nil, variables
}

func getParams(req *http.Request, variables map[string]interface{}, params []Parameter, paramType string) error {
	for _, parameter := range params {
		parameter.ParamType = paramType
		err, value := validate(getParamValue(req, paramType, parameter), parameter)
		if err != nil {
			return err
		}

		if value != nil {
			variables["req_"+paramType+"_"+parameter.Name] = value
		}
	}
	return nil
}

func getParamValue(req *http.Request, paramType string, param Parameter) interface{} {
	switch paramType {
	case "url":
		return chi.URLParam(req, param.Name)
	case "header":
		return req.Header.Get(param.Name)
	case "query":
		return req.URL.Query().Get(param.Name)
	}
	return nil
}

func validate(value interface{}, param Parameter) (error, interface{}) {
	isEmpty := false
	switch value.(type) {
	case string:
		isEmpty = (value.(string) == "")
	}

	if param.Mandatory && (value == nil || isEmpty) {
		return errors.New("missing mandatory field " + param.Name + " in " + param.ParamType), nil
	}

	if value == nil || isEmpty {
		return nil, param.Default
	}

	return nil, value

	// if value == nil || isEmpty {
	// 	if param.Default == nil {
	// 		return nil, nil
	// 	}

	// 	value = param.Default
	// }

	// // at this point value won't be nil or empty. either it has a value provided by the user
	// // or the default value
	// // Now, convert it to the proper type
	// str, _ := value.(string)

	// switch param.Type {
	// case "boolean":
	// 		b, err := strconv.ParseBool(str)
	// 		if ()
	// }

	// fmt.Println("  ============= DEFAULT  ========= ", param.Default, " and type is ", reflect.TypeOf(param.Default))
}
