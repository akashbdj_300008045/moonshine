package server

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"runtime"
	"runtime/pprof"
	"strconv"
	"time"

	"bitbucket.org/myntra/moonshine/pkg/features"
	"bitbucket.org/myntra/moonshine/pkg/logger"
	"bitbucket.org/myntra/moonshine/pkg/util/log"
	"go.uber.org/zap"

	"bitbucket.org/myntra/moonshine/pkg/graphql/client"

	gqlgen "github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/handler"
	"github.com/vektah/gqlparser/gqlerror"

	"bitbucket.org/myntra/moonshine/pkg/util/errors"

	"bitbucket.org/myntra/moonshine/pkg/graphql/resolvers"

	"bitbucket.org/myntra/moonshine/pkg/graphql"

	middlewares "bitbucket.org/myntra/moonshine/pkg/server/middleware"
	newrelic "github.com/newrelic/go-agent"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	jsoniter "github.com/json-iterator/go"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

type server struct {
	Router     chi.Router
	GqlHandler *graphql.Handler
}

func New() *server {
	return &server{
		Router:     chi.NewRouter(),
		GqlHandler: graphql.LoadSchema(graphql.Config{Resolvers: resolvers.New()})}
}

func (s *server) Start(routerConfig string, newrelicEnabled bool, newrelicApplication newrelic.Application) {
	middleware.DefaultLogger = middleware.RequestLogger(&middleware.DefaultLogFormatter{Logger: log.RequestLogger})

	r := s.Router
	r.Use(middlewares.MiddlewareCommon)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(60 * time.Second))

	// All graphql routes are handled here
	s.loadGraphqlRoutes(routerConfig, newrelicEnabled, newrelicApplication)

	// CPU/Memory profiling routes
	loadProfilingRoutes(r)

	// Healthcheck related routes
	statusRoutes(r)

	// you can load non-graphql-flow routes here
	r.Get("/enableatlas", func(w http.ResponseWriter, r *http.Request) {
		features.EnableAtlas = true
		w.WriteHeader(http.StatusOK)
		return
	})

	r.Get("/disableatlas", func(w http.ResponseWriter, r *http.Request) {
		features.EnableAtlas = false
		w.WriteHeader(http.StatusOK)
		return
	})

	r.Get("/getatlas", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(strconv.FormatBool(features.EnableAtlas)))
		return
	})

	fmt.Println("Server is listening on port: 4444")
	err := http.ListenAndServe(":4444", r)
	if err != nil {
		fmt.Println("[ERROR]: unable to start the server", err)
	}
}

func (s *server) loadGraphqlRoutes(routerConfig string, newrelicEnabled bool, newrelicApplication newrelic.Application) {
	config := readConfig(routerConfig)
	for _, route := range config.Routes {
		s.processRoute(route, newrelicApplication, newrelicEnabled)
	}
}

func (s *server) processRoute(route Route, newrelicApplication newrelic.Application, newrelicEnabled bool) {
	//this will ignore invalid routes or any route definition and load the remaining routes.
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in ", r)
		}
	}()

	s.Router.Group(func(r chi.Router) {
		r.Use(middlewares.MiddlewareHeaders)
		r.Use(middlewares.MiddlewareUser)

		var path string
		var handler http.HandlerFunc
		path, handler = wrapHandlerFunc(route.Path, s.getHandler(route), newrelicApplication, newrelicEnabled)

		r.MethodFunc(route.Method, path, handler)
	})
}

func (s *server) getHandler(route Route) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		err, variables := getVariables(req, route)
		if err != nil {
			logger.AppLogger.Error(context.TODO(), "error in parsing query variables", zap.Error(err))
			writeError(w, &errors.CustomError{
				Code:       errors.ErrorCodeInvalidInput,
				Message:    err.Error(),
				HttpStatus: http.StatusBadRequest,
			})
			return
		}

		queryIdentifier := route.Action.Value.(string)
		q := client.Queries[queryIdentifier]

		switch req.Method {
		case http.MethodGet:
			v, _ := json.Marshal(variables)
			uq := req.URL.Query()
			uq.Set("query", q)
			// TODO: check adding operationName doesn't work!
			// uq.Set("operationName", queryIdentifier)
			uq.Set("variables", string(v))
			req.URL.RawQuery = uq.Encode()
		case http.MethodPost:
			b, _ := json.Marshal(map[string]interface{}{
				"query":     q,
				"variables": variables,
				// TODO: check adding operationName doesn't work!
				// "operationName": queryIdentifier,
			})
			req.Body = ioutil.NopCloser(bytes.NewBuffer(b))
		}

		interceptor(handler.GraphQL(
			s.GqlHandler.Exec,
			handler.ErrorPresenter(
				func(ctx context.Context, e error) *gqlerror.Error {
					errPath := gqlgen.GetResolverContext(ctx).Path()
					if qerr, ok := e.(*errors.CustomError); ok {
						logger.AppLogger.Error(nil, "[ErrorPresenter] Custom Error", zap.Error(qerr), zap.Any("Error Path", errPath))
						ee := gqlerror.ErrorPathf(errPath, qerr.Message)
						ee.Extensions = map[string]interface{}{
							"code":       qerr.Code,
							"message":    qerr.Message,
							"statusCode": qerr.HttpStatus,
						}
						return ee
					}

					logger.AppLogger.Error(nil, "[ErrorPresenter] Non Custom Error", zap.Error(e), zap.Any("Error Path", errPath))
					return gqlgen.DefaultErrorPresenter(ctx, e)
				}),
		))(w, req)

	}
}

func interceptor(h http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(CustomResponseWriter{w}, r)
	}
}

// We let graphql use our CustomResponseWriter.
// Once graphql has written the response to our writer, we intercept, read, manipulate, etc,
// and then we write final response to original ResponseWriter.
type CustomResponseWriter struct {
	http.ResponseWriter
}

func (n CustomResponseWriter) Write(b []byte) (int, error) {
	var response gqlgen.Response
	err := json.Unmarshal(b, &response)
	if err != nil {
		logger.AppLogger.Error(nil, "[Response Writer] error in unmarshalling graphql response", zap.Error(err))
		writeError(n.ResponseWriter, errors.CreateError(errors.ErrorCodeUnknown, "Oops! Something failed", http.StatusInternalServerError))
		return 0, nil
	}

	// If you want to log route info
	// use: routePath := chi.RouteContext(ctx).RoutePattern()
	for _, e := range response.Errors {
		// our custom errors!
		if e.Extensions != nil {
			extn := e.Extensions
			code := int64(extn["code"].(float64))
			msg := extn["message"].(string)
			statusCode := int(extn["statusCode"].(float64))
			writeError(n.ResponseWriter, errors.CreateError(code, msg, statusCode))
			return 0, nil
		} else if e != nil {
			// other codegen related errors
			// or a resolver error where you missed returning customError
			writeError(n.ResponseWriter, errors.CreateError(errors.ErrorCodeUnknown, "Oops! Something failed", http.StatusInternalServerError))
			return 0, nil
		}
	}

	return n.ResponseWriter.Write(b[18 : len(b)-2])
}

func loadProfilingRoutes(r chi.Router) {
	r.Get("/startcpuprofile", func(w http.ResponseWriter, r *http.Request) {
		f, err := os.Create("./CpuProfileData")
		if err != nil {
			log.Err("could not create CPU profile: ", err)
			w.Write([]byte("couldn't start profiling " + err.Error()))
			return
		}
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Err("could not start CPU profile: ", err)
			w.Write([]byte("couldn't start profiling " + err.Error()))
			return
		}
		w.Write([]byte("started profiling"))
	})

	r.Get("/stopcpuprofile", func(w http.ResponseWriter, r *http.Request) {
		pprof.StopCPUProfile()
		w.Write([]byte("stopped profiling"))
	})

	r.Get("/writememprofile", func(w http.ResponseWriter, r *http.Request) {
		f, err := os.Create("./" + strconv.Itoa(int(time.Now().Unix())))
		if err != nil {
			log.Err("could not create CPU profile: ", err)
			w.Write([]byte("couldn't write mem profile " + err.Error()))
			return

		}
		runtime.GC() // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Err("could not write memory profile: ", err)
			w.Write([]byte("couldn't write mem profile " + err.Error()))
			return
		}
		f.Close()
		w.Write([]byte("wrote memory profile"))
	})
}

// wrapHandlerFunc instruments NewRelic on every route passed to it.
func wrapHandlerFunc(route string, handler http.HandlerFunc, newrelicApp newrelic.Application, enableNewRelic bool) (string, http.HandlerFunc) {
	if enableNewRelic {
		r, newRelicHandler := newrelic.WrapHandleFunc(newrelicApp, route, handler)
		return r, newRelicHandler
	}
	return route, handler
}

func writeError(w http.ResponseWriter, err *errors.CustomError) {
	if err.HttpStatus != 0 {
		w.WriteHeader(err.HttpStatus)
	}
	jerr, _ := json.Marshal(err)
	w.Write(jerr)
}

// func (s *server) getCustomHandler(route Route) http.HandlerFunc {
// 	return func(w http.ResponseWriter, req *http.Request) {
// 		w.Header().Set("Content-Type", "application/json")
// 		w.Header().Set("x-server", "gateway")

// 		err, variables := getVariables(req, route)
// 		if err != nil {
// 			writeError(w, &errors.CustomError{
// 				Code:       errors.ErrorCodeInvalidInput,
// 				Message:    err.Error(),
// 				HttpStatus: http.StatusBadRequest,
// 			})
// 			return
// 		}

// 		data, qerr := s.GqlHandler.Execute(req.Context(), route.Action.Value.(string), "", variables)
// 		if qerr != nil {
// 			writeError(w, qerr)
// 			return
// 		}

// 		w.Write(data)
// 	}
// }
