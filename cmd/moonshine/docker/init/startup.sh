. /myntra/$APP/bin/setenv.sh
echo "Starting $APP service"
# REPO=moonshine
BUILDPATH=/myntra/moonshine/src/bitbucket.org/myntra/moonshine
# mkdir -p /myntra/$APP/logs
# PIDFILE=/myntra/$APP/$APP.pid
# nohup /myntra/$APP/bin/$APP -config /myntra/$APP/$REPO.dockins.yaml -v=0 -stderrthreshold=INFO -log_dir=/myntra/$APP/logs/  \
#     & echo $! > $PIDFILE & tail -f nohup.out
PIDFILE=/myntra/moonshine/moonshine.pid
nohup $BUILDPATH/cmd/moonshine/moonshine server -config=$BUILDPATH/ops/configs/services_endpoints.json \
 -env=dockins \
 & echo $! > $PIDFILE & tail -f nohup.out
echo "$APP started!"
ps -ef | grep $APP