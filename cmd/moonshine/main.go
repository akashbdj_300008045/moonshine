package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	commonLogger "bitbucket.org/myntra/go-common-logger"
	"bitbucket.org/myntra/moonshine/pkg/config"
	"bitbucket.org/myntra/moonshine/pkg/constants"
	"bitbucket.org/myntra/moonshine/pkg/logger"
	"bitbucket.org/myntra/moonshine/pkg/server"
	"bitbucket.org/myntra/moonshine/pkg/util/log"
	newrelic "github.com/newrelic/go-agent"
	"go.uber.org/zap"
)

// TODO: Get rid of these globals. They are not used anywhere outside this package.
var EnableNewrelic bool
var NewrelicApp newrelic.Application

func main() {
	defaultPath, err := filepath.Abs("../..")
	if err != nil {
		fmt.Println(err)
		return
	}

	serverCmd := flag.NewFlagSet("server", flag.ExitOnError)
	env := serverCmd.String("env", "development", "environment, by default development")
	servicesEndpointsCfg := serverCmd.String("config", defaultPath+"/ops/configs/services_endpoints.json", "services_endpoints file location, default will be "+defaultPath+"/ops/configs/service_endpoints.json")

	if len(os.Args) == 1 {
		fmt.Println("usage: gateway <command> [<args>]")
		fmt.Println("available commands: ")
		fmt.Println("server  starts http server")
		return
	}

	switch os.Args[1] {
	case "server":
		serverCmd.Parse(os.Args[2:])
	default:
		fmt.Printf("%q is not valid command.\n", os.Args[1])
	}

	if serverCmd.Parsed() {
		logPath := defaultPath + "/logs"
		logConfig := commonLogger.DefaultConfig
		logConfig.OutFile = logPath + "/moonshine.log"
		logger.Init(logConfig)
		log.Init(logPath, *env)

		cfg, err := os.Open(*servicesEndpointsCfg)
		if os.IsNotExist(err) {
			fmt.Println("ERROR: configuration file not found at ", *servicesEndpointsCfg)
			return
		}

		err = config.Load(cfg, *env)
		if err != nil {
			fmt.Println("[ERROR] couldn't read file: ", *servicesEndpointsCfg)
			fmt.Println(err)
			logger.AppLogger.Error(context.TODO(), "couldn't read file", zap.String("path", *servicesEndpointsCfg))
			return
		}

		EnableNewrelic = strings.ToLower(os.Getenv("ENABLE_NEWRELIC")) == "true"
		if EnableNewrelic {
			newRelicconfig := newrelic.NewConfig("az-moonshine", constants.NewRelicLicense)
			//newRelicconfig.Enabled = true
			NewrelicApp, _ = newrelic.NewApplication(newRelicconfig)
		}

		routerConfig := defaultPath + "/ops/configs/router.json"
		s := server.New()
		s.Start(routerConfig, EnableNewrelic, NewrelicApp)

		// If you want to expose a single graphql endpoint,
		// comment out the above server and uncomment below one.
		//
		// http.Handle("/playground", handler.Playground("GraphQL playground", "/query"))
		// http.Handle("/query", handler.GraphQL(graphql.NewExecutableSchema(graphql.Config{Resolvers: resolvers.New()})))
		// log.Printf("connect to http://localhost:%s/ for GraphQL playground", constants.ServerPort)
		// http.ListenAndServe(":"+"4444", nil)

	}

}
