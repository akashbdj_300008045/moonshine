module bitbucket.org/myntra/moonshine

go 1.12

require (
	bitbucket.org/myntra/go-common-logger v0.0.0-20181130101356-d39f3ca8f1e8
	github.com/99designs/gqlgen v0.9.3
	github.com/bkaradzic/go-lz4 v1.0.0
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/google/uuid v1.1.1
	github.com/json-iterator/go v1.1.9
	github.com/newrelic/go-agent v2.11.0+incompatible
	github.com/nytlabs/gojsonexplode v0.0.0-20160201065013-0f3fe6bb573f
	github.com/sirupsen/logrus v1.4.2
	github.com/vektah/gqlparser v1.1.2
	github.com/yogeshpandey/gocookie-string-reader v0.0.0-20190529171657-f52d1370984c
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/net v0.0.0-20190827160401-ba9fcec4b297 // indirect
)
