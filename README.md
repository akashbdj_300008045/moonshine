## Getting Started:

1. Clone repo: `git clone git@bitbucket.org:akashbdj_300008045/moonshine.git`
2. `$ cd moonshine/cmd/moonshine`
3. `$ go run main.go`
4. Head over to `http://localhost:8080/`
5. Run your queries/mutations.

## Reporting Issues

If you think you've found a bug, or something isn't behaving the way you think it should, please raise an issue [here](https://bitbucket.org/akashbdj_300008045/moonshine/issues).

